/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flocadev.zombieland;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Rectangle;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

/**
 *
 * @author richm1325
 */

/*
 * The WorldController class is the overall logic of the game and controls the different
 * rules of the game. WorldController includes dealing with key inputs, zombie intelligence 
 * in moving to the player and collisions between player, obstacles, zombies, bullets and 
 * barricades. WorldController has access to everything it needs by calling from the MapLayout.
 */

public class WorldController<T extends AbstractNode> implements InputProcessor{

    private boolean playerUp = false, playerLeft = false, playerRight = false, playerDown = false, reload = false, playerFixingBarricade = false, upgradeGun = false;
    private MapLayout map;
    private Player player;
    private Gun gun;
    private boolean shoot;
    private boolean clickSwitch = true;
    private boolean typeSwitch = true;
    
    //Sound effects
    private File gunshotFile, clickFile;
    private Clip gunshot, clickSound;
    
    // lists of game objects
    ArrayList<Bullet> bullets = new ArrayList<Bullet>();
    ArrayList<Zombie> zombies = new ArrayList<Zombie>();
    ArrayList<Tile> obstacles = new ArrayList<Tile>();
    ArrayList<Tile> barricades = new ArrayList<Tile>();
    
    //pathfinding variables
    private T[][] nodes;
    private List<T> openList;
    private List<T> closedList;
    private NodeBuilder nodeBuilder;
    private int width, height;

    boolean done = false;
    
    private int zombieKillPoints;
    private double zombieAttackPlayer;
    private double revivingHealth;
    private double zombieAttackBarricade;
    private int time;
    
    //controls which screen is being displayed
    private int level; // level 0 = main menu, level 1 = control screen, level 2 = intructions, level 3 = main game, level 4 = pause menu, level 5 = game over
    
    public WorldController(MapLayout map, int width, int height, NodeBuilder nodeBuilder)
    {
        this.map = map;
        this.player = map.getPlayer();
        map.nextWave();
        this.zombies = map.getSpawningZombies();    //The actual zombies that spawn
        this.bullets = map.getCreatedBullets();
        this.obstacles = map.getCreatedObstacles();
        this.barricades = map.getCreatedBarricades();
        this.gun = map.getGun();this.nodeBuilder = nodeBuilder;        
        nodes = (T[][]) new AbstractNode[width][height];
        this.width = width - 1;
        this.height = height - 1;
        this.zombieKillPoints = 10;
        this.zombieAttackPlayer = 0.1;
        this.zombieAttackBarricade = 1;
        this.revivingHealth = 1;
        this.time = 0;
        this.level = 0;
        initializeEmptyNodes();
        
        //For sound effects:
        gunshotFile = new File("sounds/gunshot.wav");
        clickFile = new File("sounds/click.wav");   //used when player is out of ammo
    }
    
    public int getLevel()
    {
        return level;
    }
    
    //creates nodes for each tile in the map
    private void initializeEmptyNodes() {
        for (int i = 0; i <= this.width; i++) {
            for (int j = 0; j <= this.height; j++) {
                nodes[i][j] = (T) new ExampleNode(i,j);
            }
        }
    }
    
    // this method is made for getting any keys that the user inputs
    private void processInput()
    {
        // pressing "A" key
        if(playerLeft)
        {
            // move the player to the left
            player.setXVel(-Player.SPEED);
        }
        
        // pressing "D" key
        if(playerRight)
        {
            // move the player to the right
            player.setXVel(Player.SPEED);
        }
        
        // pressing "W" key
        if(playerUp)
        {
            // move the player up
            player.setYVel(Player.SPEED);
        }
        
        // pressing "S" key
        if(playerDown)
        {
            // move the player down
            player.setYVel(-Player.SPEED);
        }
        
        //pressing "shift"
        if (upgradeGun) {
            // if shift is pressed when the player can get the next upgrade
            // and the player has enough score to 'buy' the upgrade
            if (map.getUpgrade() && player.getScore() >= map.upgradeCost())
                // give the player his upgraded gun
                upgradeGun(map.getUpgradeLevel());
        }
        
        // if the player is pressing "R" key and has no more bullets in clip
        if(reload == true && player.getGun().getBulletsInClip() == 0)
        {
            // essentially reload the gun
            player.getGun().resetReload();
            player.getGun().setShotDelay();
        }
        
        // player fixing unbroken, damaged barricade
        Iterator<Tile> iBarricades = barricades.iterator();
            while (iBarricades.hasNext()) {
                Barricade currentBarricade =  (Barricade) iBarricades.next();
                // if player is pressing "SPACE" key and is touching a barricade that is damaged
                if (playerFixingBarricade == true && player.isTouchingBarricade(currentBarricade) && currentBarricade.getHealth() < 100) {
                    // give the barricade health
                    currentBarricade.addHealth(revivingHealth);
                }
        }
        
        // if the player is or is not pressing right or left at the same time
        if(!playerRight && !playerLeft || playerRight && playerLeft)
        {
            // do not move left or right
            player.setXVel(0);
        }
        
        // if the player is or is not pressing up or down at the same time
        if(!playerUp && !playerDown || playerUp && playerDown)
        {
            // do not move up for down
            player.setYVel(0);
        }
        
        // if the left mouse is clicked
        if(shoot == true)  
        {
            // if the player still has a bullet in the clip and the shot delay is finished
            if (player.getGun().getBulletsInClip() > 0 && player.getGun().getShotDelay() <= 0) {
                //Gunshot
                try {
                    // gun shot sounds
                    AudioInputStream AIS_Song = AudioSystem.getAudioInputStream(gunshotFile);
                    gunshot = AudioSystem.getClip();
                    gunshot.open(AIS_Song);
                    gunshot.start();
                }catch(Exception e) {
                    e.printStackTrace();
                }
            }
            // if the clip is empty and the player is trying to shoot
            else if (player.getGun().getBulletsInClip() <= 0)   {
                try {
                    //Out of ammo 'click' sound
                    AudioInputStream AIS_Song = AudioSystem.getAudioInputStream(clickFile);
                    clickSound = AudioSystem.getClip();
                    clickSound.open(AIS_Song);
                    clickSound.start();
                }catch(Exception e) {
                    e.printStackTrace();
                }
            }
            // create the bullet
            map.createBullet();
            
            // if the gun is not automatic then make shoot false to stop the
            // player from keep shooting
            if(player.getGun().isAutomatic() == false)
            {
                shoot = false;
            }
        }
    }
    
    // this method handles the collisions between player and zombie with walls,
    // removing bullets, bullet and zombie collisions, bullet and obstacle collisons, 
    // zombie and barricade collisons and zombie and player collisions
    public void checkCollisions()
    {
        // player's position coordinates
        int playerX = (int)player.getX();
        int playerY = (int)player.getY();
        int playerEndX = (int)(player.getX() + player.SIZE);
        int playerEndY = (int)(player.getY() + player.SIZE);
        
        // handling player collisions
        for(int i = playerX; i <= playerEndX; i++)
        {
            for(int j = playerY; j <= playerEndY; j++)
            {
                // collision
                if(map.getTile(i, j) != null)
                {
                    // hit something calculate the overlap
                    float playerOverX = 0;
                    float playerOverY = 0;
                    
                    if(playerX < i) // collision right
                    {
                        playerOverX = player.getX() + Player.SIZE - i;
                    }else // left collision
                    {
                        playerOverX = i + Player.SIZE - player.getX();
                    }
                    
                    if(playerY < j) // collision on top
                    {
                        playerOverY = player.getY() + Player.SIZE - j;
                    }else // collision on bottom
                    {
                        playerOverY = j + Player.SIZE - player.getY();
                    }
                    
                    // deal with the overlaps
                    // the axis with the least amount of overlap
                    if(playerOverY < playerOverX)
                    {
                        player.setYVel(0);
                        if(playerY < j) // top
                        {
                            player.setY(player.getY() - playerOverY - 0.005f);
                        }else // hit bottom
                        {
                            player.setY(player.getY() + playerOverY + 0.005f);
                        }
                    }else
                    {
                        player.setXVel(0);
                        if(playerX < i) // right side
                        {
                            player.setX(player.getX() - playerOverX - 0.005f);
                        }else // left side
                        {
                            player.setX(player.getX() + playerOverX + 0.005f);
                        }
                    }
                }
            }
        }
        
        //Zombie collisions with obstacles and barricades(array list):
        for (Zombie z: zombies) {
            int zX = (int)z.getX();
            int zY = (int)z.getY();
            int zEndX = (int)(z.getX() + Zombie.SIZE);
            int zEndY = (int)(z.getY() + Zombie.SIZE);
            if(z.getState() != Zombie.State.BREAKING)
            {
                for(int x = zX; x <= zEndX; x++)
                {
                    for(int y = zY; y <= zEndY; y++)
                    {
                        // collision
                        if(map.getTile(x, y) != null)
                        {
                            //Determine whether the object is a barricade
                            if (map.getTile(x, y).getType() == Tile.Type.BARRICADE)
                                z.setState(Zombie.State.BREAKING);  //Zombie begins breaking barricade

                            // hit something calculate the overlap
                            float zombieOverX = 0;
                            float zombieOverY = 0;

                            if(zX < x) // collision right
                            {
                                zombieOverX = z.getX() + Zombie.SIZE - x;
                            }else // left collision
                            {
                                zombieOverX = x + Zombie.SIZE - z.getX();
                            }

                            if(zY < y) // collision on top
                            {
                                zombieOverY = z.getY() + Zombie.SIZE - y;
                            }else // collision on bottom
                            {
                                zombieOverY = y + Zombie.SIZE - z.getY();
                            }

                            // deal with the overlaps
                            // the axis with the least amount of overlap
                            if(zombieOverY < zombieOverX)
                            {
                                if(zY < y) // top
                                {
                                    z.setY(z.getY() - zombieOverY);
                                    if(player.getX() < z.getX())
                                    {
                                        z.setX(z.getX() - 0.005f);
                                    }else if(player.getX() > z.getX())
                                    {
                                        z.setX(z.getX() + 0.005f);
                                    }
                                }else // hit bottom
                                {
                                    z.setY(z.getY() + zombieOverY);
                                    if(player.getX() < z.getX())
                                    {
                                        z.setX(z.getX() - 0.005f);
                                    }else if(player.getX() > z.getX())
                                    {
                                        z.setX(z.getX() + 0.005f);
                                    }
                                }
                            }else
                            {
                                if(zX < x) // right side
                                {
                                    z.setX(z.getX() - zombieOverX);
                                    if(player.getY() < z.getY())
                                    {
                                        z.setY(z.getY() - 0.005f);
                                    }else if(player.getY() > z.getY())
                                    {
                                        z.setY(z.getY() + 0.005f);
                                    }
                                }else // left side
                                {
                                    z.setX(z.getX() + zombieOverX);
                                    if(player.getY() < z.getY())
                                    {
                                        z.setY(z.getY() - 0.005f);
                                    }else if(player.getY() > z.getY())
                                    {
                                        z.setY(z.getY() + 0.005f);
                                    }
                                }
                            }

                        }
                    }
                }
            }
        }
        
        //Zombie collisions with other zombies
        for (int i = 0; i < zombies.size(); i++)    {   //Go through each zombie in the array of zombies
            Zombie z = zombies.get(i);  //Pick the individual zombie from the for loop
            //Set the boundaries of the zombie:
            Rectangle zomb1 = new Rectangle(z.getX(), z.getY(), Zombie.SIZE, Zombie.SIZE);
            
            //Compare each x and y value within the boundary to the x and y value of
            //every other zombie in the array
            if(z.getXVel() != 0 || z.getYVel() != 0)
            {
                for (int j = i + 1; j < zombies.size(); j++)    {
                    Zombie z2 = zombies.get(j); //the other zombies in the array
                    //Set the boundaries of the zombie:
                    Rectangle zomb2 = new Rectangle(z2.getX(), z2.getY(), Zombie.SIZE, Zombie.SIZE);

                    //Handle collision
                    if(zomb1.overlaps(zomb2))   {
                        float overX = 0;
                        float overY = 0;
                        boolean above, rightOf; //makes the job easier

                        //X overlap
                        if (zomb2.x > zomb1.x)  {   //z2 is to the right
                            overX = zomb1.x + zomb1.width - zomb2.x;
                            rightOf = true;
                        }
                        else    {   //z2 is to the left
                            overX = zomb2.x + zomb2.width - zomb1.x;
                            rightOf = false;
                        }

                        //Y overlap
                        if (zomb2.y > zomb1.y)  {   //z2 is above
                            overY = zomb1.y + zomb1.height - zomb2.y;
                            above = true;
                        }
                        else    {   //z2 is below
                            overY = zomb2.y + zomb2.height - zomb1.y;
                            above = false;
                        }

                        //Deal with overlap
                        //The axis with the least amount of overlap
                        if (overY < overX)  {
                            if (above)
                                z.setY(z.getY() - overY);
                            else
                                z.setY(z.getY() + overY);
                        }
                        else    {
                            if (rightOf)
                                z.setX(z.getX() - overX);
                            else
                                z.setX(z.getX() + overX);
                        }
                    }
                }
            }
        }
        
        //Remove bullets that fly off of the screen
        Iterator<Bullet> i1 = bullets.iterator();
        while (i1.hasNext()) {
            Bullet b = i1.next();

            if (b.getX() < 0 || b.getX() > map.getWidth() || b.getY() < 0 || b.getY() > map.getHeight())   {
                i1.remove();
            }
        }
            
        // determine if the player bullets have hit an enemy object
        // if yes, enemy is dead
        Iterator<Bullet> iBullets = bullets.iterator();
        while (iBullets.hasNext()) {
            Bullet currentBullet = iBullets.next();

            Iterator<Zombie> iZombie = zombies.iterator();
            while (iZombie.hasNext()) {
                Zombie currentZombie = iZombie.next();
                if (currentZombie.isHitByBullet( currentBullet )) {
                    // zombie and bullet are dead
                    iBullets.remove();
                    currentZombie.hit(gun.getDamage());

                    // The bullet can only kill 1 zombie 
                    // so don't look for more
                    break;
                }
            }
        }
        
        // checking if bullet has hit an obstacle
        Iterator<Bullet> iBullets2 = bullets.iterator();
            while (iBullets2.hasNext()) {
                Bullet currentBullet = iBullets2.next();

            Iterator<Tile> iObstacles = obstacles.iterator();
            while (iObstacles.hasNext()) {
                Obstacle currentObstacle =  (Obstacle) iObstacles.next();
                if (currentObstacle.isHitByBullet(currentBullet)) {
                    // bullet is dead
                    iBullets2.remove();

                    break;
                }
            }
        }
            
        // checking if a zombie is breaking down a barricade
        Iterator<Zombie> iZombies2 = zombies.iterator();
            while (iZombies2.hasNext()) {
                Zombie currentZombie = iZombies2.next();

            Iterator<Tile> iBarricades = barricades.iterator();
            while (iBarricades.hasNext()) {
                Barricade currentBarricade =  (Barricade) iBarricades.next();
                if (currentZombie.isTouchingBarricade(currentBarricade) && currentZombie.getState() == Zombie.State.BREAKING && currentBarricade.getHealth() > 0) {
                    // kill the barricades health
                    currentBarricade.subtractHealth(zombieAttackBarricade);
                    break;
                }else if(currentZombie.isTouchingBarricade(currentBarricade) && currentBarricade.getHealth() == 0)
                {
                    // this is the code that allows the zombies to get in comment this out to get them to come inside
                    // remove the barricade from the map
                    for(int x = 0; x < map.getWidth()-1; x++)
                    {
                        for(int y = 0; y < map.getHeight()-1; y++)
                        {
                            if(map.getTile(x, y) == (currentBarricade))
                            {
                                // remove barricade object from grid
                                map.setTile(x, y, null);
                            }
                        }
                    }
                    // remove the barricade from the set of barricades
                    iBarricades.remove();
                    // zombie's state is now inside so move after player
                    currentZombie.setState(Zombie.State.INSIDE);
                }
            }
        }
            
        // checking if a zombie is touching the player
        Iterator<Zombie> iZombies3 = zombies.iterator();
            while (iZombies3.hasNext()) {
                Zombie currentZombie = iZombies3.next();

                // if it is than zombie attacks player and removes player health
                if (currentZombie.isHitByPlayer(player)) {
                    currentZombie.attackPlayer();
                    break;
                }
        }
    }
    
    // this method handles the collisons between two zombies
    public void handleZombieCollision(Zombie z, Rectangle zomb1, Rectangle zomb2) {
        float overX = 0;
        float overY = 0;
        boolean above, rightOf; //makes the job easier

        //X overlap
        if (zomb2.x > zomb1.x)  {   //z2 is to the right
            overX = zomb1.x + zomb1.width - zomb2.x;
            rightOf = true;
        }
        else    {   //z2 is to the left
            overX = zomb2.x + zomb2.width - zomb1.x;
            rightOf = false;
        }

        //Y overlap
        if (zomb2.y > zomb1.y)  {   //z2 is above
            overY = zomb1.y + zomb1.height - zomb2.y;
            above = true;
        }
        else    {   //z2 is below
            overY = zomb2.y + zomb2.height - zomb1.y;
            above = false;
        }

        //Deal with overlap
        //The axis with the least amount of overlap
        if (overY < overX)  {
            if (above)
                z.setY(z.getY() - overY);
            else
                z.setY(z.getY() + overY);
        }
        else    {
            if (rightOf)
                z.setX(z.getX() - overX);
            else
                z.setX(z.getX() + overX);
        }
    }
    
    
    //PATHFINDING CODE BEGINS
    
    public final T getNode(int x, int y) {
        return nodes[x][y];
    }
    
    //finds a path between the zombie and the player
    public final List<T> findPath(int zX, int zY, int pX, int pY) {
            openList = new LinkedList<T>();
            closedList = new LinkedList<T>();
            
            openList.add(nodes[zX][zY]); // add starting node (where the zombie is) to open list
            for (int i = 0; i < map.getWidth(); i++) {
                for (int j = 0; j < map.getHeight(); j++) {
                    
                    //only sets ground tiles to "walkable"
                    //only walkable tiles will be considered in the pathfinding
                    if(map.getTile(i, j) == null)
                    {
                        nodes[i][j].setWalkable(true);
                    }else
                    {
                        nodes[i][j].setWalkable(false);
                    }
                }
            }
            done = false;
        
            T current;
            while (!done) {
            current = lowestFInOpen(); // get node with lowest fCosts from openList
            closedList.add(current); // add current node to closed list
            openList.remove(current); // delete current node from open list

            if ((current.getxPosition() == pX)
                    && (current.getyPosition() == pY)) { // found goal
                return calcPath(nodes[zX][zY], current);
            }
            
            // for all adjacent nodes:
            List<T> adjacentNodes = getAdjacent(current);
            for (int i = 0; i < adjacentNodes.size(); i++) {
                T currentAdj = adjacentNodes.get(i);
                if (!openList.contains(currentAdj)) { // node is not in openList
                    currentAdj.setPrevious(current); // set current node as previous for this node
                    currentAdj.sethCosts(nodes[pX][pY]); // set h costs of this node (estimated costs to goal)
                    currentAdj.setgCosts(current); // set g costs of this node (costs from start to this node)
                    openList.add(currentAdj); // add node to openList
                } else { // node is in openList
                    if (currentAdj.getgCosts() > currentAdj.calculategCosts(current)) { // costs from current node are cheaper than previous costs
                        currentAdj.setPrevious(current); // set current node as previous for this node
                        currentAdj.setgCosts(current); // set g costs of this node (costs from start to this node)
                    }
                }
            }
            
            if (openList.isEmpty()) { //if no path exists
                return new LinkedList<T>(); // return empty list
            }
            
        }
        return null; 
    }
    
    private T lowestFInOpen() {
        //starts with the first node in the open list, calling it cheapest
        T cheapest = openList.get(0);
        for (int i = 0; i < openList.size(); i++) {
            //if a later node is cheaper, it is set to the cheapest node
            if (openList.get(i).getfCosts() < cheapest.getfCosts()) {
                cheapest = openList.get(i);
            }
        }
        return cheapest;
    }
    
    
    private List<T> calcPath(T start, T goal) {
        LinkedList<T> path = new LinkedList<T>();

        T curr = goal;
        boolean done = false;
        
        for(Zombie z: zombies)
        {
            //if the zombie is already on the same tile as the player
            if(z.isHitByPlayer(player))
            {
                if(z.getX() < player.getX())
                    {
                        z.setXVel(2f);
                    }
                if(z.getX() > player.getX())
                    {
                        z.setXVel(-2f);
                    }
                if(z.getY() < player.getY())
                    {
                        z.setYVel(2f);
                    }
                if(z.getY() > player.getY())
                    {
                        z.setYVel(-2f);
                    }
                return path;
            }else
            {
                while (done == false) {
                path.addFirst(curr);
                if(curr.equals(start))
                {
                    
                }else
                {
                curr = (T) curr.getPrevious();
                }
                if (curr.equals(start)) {
                    done = true;
                    }

                }
            }
        }
        return path;
    }
    
    //gets the nodes that are touching the current node
    private List<T> getAdjacent(T node) {
        int x = node.getxPosition();
        int y = node.getyPosition();
        List<T> adj = new LinkedList<T>();

        T temp;
        if (x > 0) {
            temp = this.getNode((x - 1), y);
            if (temp.isWalkable() && !closedList.contains(temp)) {
                adj.add(temp);
            }
        }

        if (x < map.getWidth() - 1) {
            temp = this.getNode((x + 1), y);
            if (temp.isWalkable() && !closedList.contains(temp)) {
                adj.add(temp);
            }
        }

        if (y > 0) {
            temp = this.getNode(x, (y - 1));
            if (temp.isWalkable() && !closedList.contains(temp)) {
                adj.add(temp);
            }
        }

        if (y < map.getHeight() - 1) {
            temp = this.getNode(x, (y + 1));
            if (temp.isWalkable() && !closedList.contains(temp)) {
                adj.add(temp);
            }
        }

        // add nodes that are diagonaly adjacent
        if (x < map.getWidth() - 1 && y < map.getHeight() - 1) {
            temp = this.getNode((x + 1), (y + 1));
            if (temp.isWalkable() && !closedList.contains(temp)) {
                adj.add(temp);
            }
        }

        if (x > 0 && y > 0) {
            temp = this.getNode((x - 1), (y - 1));
            if (temp.isWalkable() && !closedList.contains(temp)) {
                adj.add(temp);
            }
        }

        if (x > 0 && y < map.getHeight() - 1) {
            temp = this.getNode((x - 1), (y + 1));
            if (temp.isWalkable() && !closedList.contains(temp)) {
                adj.add(temp);
            }
        }

        if (x < map.getWidth() - 1 && y > 0) {
            temp = this.getNode((x + 1), (y - 1));
            if (temp.isWalkable() && !closedList.contains(temp)) {
                adj.add(temp);
            }
        }
        
        return adj;
    }
    
    // this method uses the path to move to the player
    public void moveToPlayer() {
        
    Iterator<Zombie> zombiePathfinding = zombies.iterator();
        while (zombiePathfinding.hasNext()) 
        {
            Zombie currentZombie = zombiePathfinding.next();

            if(currentZombie.getState() == Zombie.State.INSIDE)
            {
                //calls the findpath method for the iterator of zombies
                List<T>path = findPath((int)currentZombie.getX(), (int)currentZombie.getY(), (int)player.getX(), (int)player.getY());
                for (int i = 0; i < path.size(); i++) {
                    //1
                    if(currentZombie.getX() < path.get(0).getxPosition() && currentZombie.getY() < path.get(0).getyPosition() && map.getTile(path.get(0).getxPosition(), path.get(0).getyPosition()-1) == null)//if zombie is to the left of the next position in the path
                    {
                        currentZombie.setXVel(Zombie.SPEED);
                    }
                    //2
                    if(currentZombie.getX() > path.get(0).getxPosition() && currentZombie.getY() < path.get(0).getyPosition() && map.getTile(path.get(0).getxPosition(), path.get(0).getyPosition()-1) == null)//if zombie is to the right of the next position in the path
                    {
                        currentZombie.setXVel(-Zombie.SPEED);
                    }

                    //3
                    if(currentZombie.getX() < path.get(0).getxPosition() && currentZombie.getY() > path.get(0).getyPosition() && map.getTile(path.get(0).getxPosition(), path.get(0).getyPosition()+1) == null)//if zombie is to the left of the next position in the path
                    {
                        currentZombie.setXVel(Zombie.SPEED);
                    }
                    //4
                    if(currentZombie.getX() > path.get(0).getxPosition() && currentZombie.getY() > path.get(0).getyPosition() && map.getTile(path.get(0).getxPosition(), path.get(0).getyPosition()+1) == null)//if zombie is to the right of the next position in the path
                    {
                        currentZombie.setXVel(-Zombie.SPEED);
                    }

                    //5
                    if(currentZombie.getY() < path.get(0).getyPosition() && currentZombie.getX() > path.get(0).getxPosition() && map.getTile(path.get(0).getxPosition()+1, path.get(0).getyPosition()) == null)//if zombie is below the next position in the path
                    {
                        currentZombie.setYVel(Zombie.SPEED);
                    }
                    //6
                    if(currentZombie.getY() < path.get(0).getyPosition() && currentZombie.getX() < path.get(0).getxPosition() && map.getTile(path.get(0).getxPosition()-1, path.get(0).getyPosition()) == null)//if zombie is below the next position in the path
                    {
                        currentZombie.setYVel(Zombie.SPEED);
                    }
                    //7
                    if(currentZombie.getY() > path.get(0).getyPosition() && currentZombie.getX() < path.get(0).getxPosition() && map.getTile(path.get(0).getxPosition()-1, path.get(0).getyPosition()) == null)//if zombie is below the next position in the path
                    {
                        currentZombie.setYVel(-Zombie.SPEED);
                    }
                    //8
                    if(currentZombie.getY() > path.get(0).getyPosition() && currentZombie.getX() > path.get(0).getxPosition() && map.getTile(path.get(0).getxPosition()+1, path.get(0).getyPosition()) == null)//if zombie is below the next position in the path
                    {
                        currentZombie.setYVel(-Zombie.SPEED);
                    }

                }
            }
        }
    }
    
    // this method updates all the score of the game and calls 
    // the update methods in all of the objects' classes to update 
    // the objects state and position based on the rules of the game
    public void update(float delta)
    {
        // checks to see if any keys are pressed
        processInput();
        
        // time of the game is increased by one
        time += 1;
        
        //Update the zombies
        Iterator<Zombie> iZombies = zombies.iterator();
        while (iZombies.hasNext())  {
            Zombie currentZombie = iZombies.next();
            // if zombie is dead then remove it and give the player points
            if (currentZombie.getHealth() <= 0) {
                iZombies.remove();
                player.addScore(zombieKillPoints);
            }
            else
                // if the zombie is not dead update him
                currentZombie.update(delta);
            
            // if time is 100 and the player is not getting attacked player regains health
            if(time > 100 && player.getHealth() < 100 && currentZombie.isHitByPlayer(player) == false)
            {
                player.addHealth(revivingHealth);
                time = 1;
            }
        }
        
        // Update the barricades
        Iterator<Tile> iBarricades = barricades.iterator();
            while (iBarricades.hasNext()) {
                Barricade currentBarricade =  (Barricade) iBarricades.next();
                currentBarricade.update();
            }
        
        // Update the bullets
        for(Bullet b: bullets)  {
            b.update(delta);
        }
        
        //Essentially, update everything in the game that needs updating
        
        map.update(delta);
        
        player.update(delta);
        
        gun.update(delta);
        
        moveToPlayer();
        
        checkCollisions();
        
        //if player dies switch to the dead screen
        if(player.getHealth() <= 0)
        {
            level = 5;
        }
    }
    
    // this method checks if a gun upgrade is availible for the player
    public void upgradeGun(int upgradeLevel)    {
        if (upgradeLevel == 1)
            player.giveGun(Gun.Type.Dual_Colt1911);
        else if (upgradeLevel == 2)
            player.giveGun(Gun.Type.M16A1);
        else if (upgradeLevel == 3)
            player.giveGun(Gun.Type.AK47);
        
        // if the player upgrades than remove points for the cost of the new gun
        player.addScore(-map.upgradeCost());
        map.finishUpgrade();
    }
    
    @Override
    public boolean keyDown(int keycode) {
        if(keycode == Input.Keys.A)
        {
            playerLeft = true;
        }
        if(keycode == Input.Keys.D)
        {
            playerRight = true;
        }
        if(keycode == Input.Keys.W)
        {
            playerUp = true;
        }
        if(keycode == Input.Keys.S)
        {
            playerDown = true;
        }
        if(keycode == Input.Keys.R)
        {
            reload = true;
        }
        if(keycode == Input.Keys.SPACE)
        {
            playerFixingBarricade = true;
        }
        if (keycode == Input.Keys.SHIFT_LEFT)   {
            upgradeGun = true;
        }
        if(keycode == Input.Keys.ENTER && level == 3 && typeSwitch == true)
        {
            level = 4;
            typeSwitch = false;
        }
        if(keycode == Input.Keys.ENTER && level == 4 && typeSwitch == true)
        {
            level = 3;
            typeSwitch = false;
        }
        return true;
    }

    @Override
    public boolean keyUp(int keycode) {
        if(keycode == Input.Keys.A)
        {
            playerLeft = false;
        }
        if(keycode == Input.Keys.D)
        {
            playerRight = false;
        }
        if(keycode == Input.Keys.W)
        {
            playerUp = false;
        }
        if(keycode == Input.Keys.S)
        {
            playerDown = false;
        }
        if(keycode == Input.Keys.R)
        {
            reload = false;
        }
        if(keycode == Input.Keys.SPACE)
        {
            playerFixingBarricade = false;
        }
        if (keycode == Input.Keys.SHIFT_LEFT)   {
            upgradeGun = false ;
        }
        if(keycode == Input.Keys.ENTER && typeSwitch == false)
        {
            typeSwitch = true;
        }
        return true;
    }

    @Override
    public boolean keyTyped(char character) {
       return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        
        //menu screen
        if(level == 0&& clickSwitch == true)
        {
            //start game button
            if(screenX >= MapRenderer.camWidth*MapRenderer.ppuX/2-80 && screenX <= MapRenderer.camWidth*MapRenderer.ppuX/2+ 100 && screenY >= MapRenderer.camHeight*MapRenderer.ppuY/2-35 && screenY <= MapRenderer.camHeight*MapRenderer.ppuY/2+15)
            {
                level = 3;
                clickSwitch = false;
            }
            
            //tutorial button
            if(screenX >= MapRenderer.camWidth*MapRenderer.ppuX/2-65 && screenX <= MapRenderer.camWidth*MapRenderer.ppuX/2+ 85 && screenY >= MapRenderer.camHeight*MapRenderer.ppuY/2+85 && screenY <= MapRenderer.camHeight*MapRenderer.ppuY/2+135)
            {
                level = 2;
                clickSwitch = false;
            }
            
            //controls button
            if(screenX >= MapRenderer.camWidth*MapRenderer.ppuX/2-65 && screenX <= MapRenderer.camWidth*MapRenderer.ppuX/2+ 85 && screenY >= MapRenderer.camHeight*MapRenderer.ppuY/2+25 && screenY <= MapRenderer.camHeight*MapRenderer.ppuY/2+75)
            {
                level = 1;
                clickSwitch = false;
            }
        }
        
        //control screen
        if(level == 1 && clickSwitch == true)
        {
            //main menu button
            if(screenX >= 3*MapRenderer.camWidth*MapRenderer.ppuX/4 && screenX <= 3*MapRenderer.camWidth*MapRenderer.ppuX/4 +180 && screenY >= 9*MapRenderer.camHeight*MapRenderer.ppuY/10-15 && screenY <= 9*MapRenderer.camHeight*MapRenderer.ppuY/10+35)
            {
                level = 0;
                clickSwitch = false;
            }
        }
        
        //tutorial screen
        if(level == 2 && clickSwitch == true)
        {
            //start game button
            if(screenX >= MapRenderer.camWidth*MapRenderer.ppuX/2-80 && screenX <= MapRenderer.camWidth*MapRenderer.ppuX/2+ 100 && screenY >= MapRenderer.camHeight*MapRenderer.ppuY/2-35 && screenY <= MapRenderer.camHeight*MapRenderer.ppuY/2+15)
            {
                level = 0;
                clickSwitch = false;
            }
        }
        
        //main game
        if(level == 3 && button == 0)
            {
                if(clickSwitch == true)
                    {
                    shoot = true;
                    clickSwitch = false;
                    }
                else if (gun.isAutomatic())   { //You can shoot automatic guns whilst holding the mouse btn
                    shoot = true;
                }
                else
                    shoot = false;
            }
        
        //pause menu
         if(level == 4 && clickSwitch == true)
         {
             //resume game button
            if(screenX >= MapRenderer.camWidth*MapRenderer.ppuX/2-80 && screenX <= MapRenderer.camWidth*MapRenderer.ppuX/2+ 100 && screenY >= MapRenderer.camHeight*MapRenderer.ppuY/2-35 && screenY <= MapRenderer.camHeight*MapRenderer.ppuY/2+15)
            {
                level = 3;
                clickSwitch = false;
            }
            
            //main menu button
            if(screenX >= MapRenderer.camWidth*MapRenderer.ppuX/2-65 && screenX <= MapRenderer.camWidth*MapRenderer.ppuX/2+ 85 && screenY >= MapRenderer.camHeight*MapRenderer.ppuY/2+25 && screenY <= MapRenderer.camHeight*MapRenderer.ppuY/2+75)
            {
                level = 0;
                clickSwitch = false;
            }
         }
        
         //gameover menu
         if(level == 5 && clickSwitch == true)
         {
            //main menu button
            if(screenX >= 3*MapRenderer.camWidth*MapRenderer.ppuX/4 && screenX <= 3*MapRenderer.camWidth*MapRenderer.ppuX/4 +180 && screenY >= 9*MapRenderer.camHeight*MapRenderer.ppuY/10-15 && screenY <= 9*MapRenderer.camHeight*MapRenderer.ppuY/10+35)
            {
                level = 0;
                clickSwitch = false;
            }
         }
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        clickSwitch = true;
        if(level == 3)
        {
            shoot = false;
        }
        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
    
}
