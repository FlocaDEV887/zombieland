/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flocadev.zombieland;

import com.badlogic.gdx.math.Rectangle;

/**
 *
 * @author nickm2422
 */

/*
 * A subclass of Tile; this a tile in which the player or zombie cannot pass or
 * shoot through. It also cannot be broken. When bullet overlaps with an obstacle
 * the bullet is destroyed
 */

public class Obstacle extends Tile{
    
    public Obstacle(float x, float y)
    {
        super(x, y, Type.OBSTACLE);
    }
    
    public Obstacle(float x, float y,  Type objectType)
    {
        super(x, y, objectType );
    }
    
    //Collision rectangle
    public Rectangle getRect(){
        Rectangle rect;
        rect = new Rectangle(x, y, SIZE, SIZE);
        return(rect);
    }
    
    // determines if an obstacle is hit by a bullet
    public boolean isHitByBullet(Bullet b){
        Rectangle objRect;
        Rectangle obj2Rect;
        boolean collision;

        // get the rectangle for obstacle
        objRect = this.getRect();
        
        // get the rectangle for the bullet
        obj2Rect = b.getRect();
        
        // determine if the obstacle and bullet collide/overlap
        collision = objRect.overlaps( obj2Rect );

        return(collision);
    }
}
