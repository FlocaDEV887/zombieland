/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flocadev.zombieland;

/**
 *
 * @author nickm2422
 */
public interface NodeBuilder {
    public AbstractNode createNode(int x, int y);
    
}