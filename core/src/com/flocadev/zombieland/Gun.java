/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flocadev.zombieland;

/**
 *
 * @author flocc5441
 */

/*
 * The gun class contains all the specifics of the gun that the player posesses
 * (see variables). This class controls certain elements such as reloading, when
 * a player is allowed to shoot, etc.. The shooting itself is going to be done in
 * the Player class. Instead of having this as a master class, with each gun type
 * (pistol, shotgun, automatic, etc.) as a sub class, an enumerator can be used
 * instead, which determines the specifications of the gun.
 */

public class Gun {
    private float clipSize, bulletsInClip;
    private float fireRate;     //the greater the value, the slower the gun
    private float shotDelay;
    private float reloadDelay;
    private float reloadTimer;
    private float damage;
    private boolean automatic;
    
    //The gun that is being used
    public enum Type    {
        Colt1911, Dual_Colt1911, M16A1, AK47
    }
    
    private Type type;
    
    public Gun(Type type)    {
        reloadTimer = 0;    //Reset the reload timer
        shotDelay = 0;      //You can shoot the moment you get the gun
        this.type = type;
        setSpecs();
    }
    
    //Set the technical specifications (see variables) based on the type of gun
    private void setSpecs() {
        if (type == Type.Colt1911) {
            clipSize = 7;
            bulletsInClip = 7;
            fireRate = 15;
            reloadDelay = 120;
            damage = 55;
            automatic = false;
        }
        else if (type == Type.Dual_Colt1911)    {
            clipSize = 14;
            bulletsInClip = 14;
            fireRate = 10;
            reloadDelay = 120;
            damage = 55;
            automatic = false;
        }
        else if (type == Type.M16A1)    {
            clipSize = 20;
            bulletsInClip = 20;
            fireRate = 8;   
            reloadDelay = 120;
            damage = 40;
            automatic = true;
        }
        else if (type == Type.AK47) {
            clipSize = 30;
            bulletsInClip = 30;
            fireRate = 6;   
            reloadDelay = 120;
            damage = 70;
            automatic = true;
        }
    }
    
    //When shot, set the delay to the fireRate
    public void setShotDelay()   {
        shotDelay = fireRate;
    }
    
    //Reset the reload timer (i.e. the player is reloading)
    public void resetReload()    {
        reloadTimer = reloadDelay;
    }
    
    public void update(float delta) {
        //Handle fire rate of gun
        if (shotDelay > 0)  
            shotDelay--;
        //Handle reloading of gun
        if (reloadTimer > 0)
        {
            reloadTimer--;
            if(reloadTimer == 0)
                {
                    reloadBulletsInClip();
                }
        }
        
    }
    
    //Getters:
    
    public float getClipSize()  {
        return clipSize;
    }
    public float getBulletsInClip() {
        return bulletsInClip;
    }
    //Used when Player shoots
    public float setBulletsInClip() {
        bulletsInClip -= 1;
        return bulletsInClip;
    }
    
    public float reloadBulletsInClip() {
        bulletsInClip = clipSize;
        return bulletsInClip;
    }
    public float getShotDelay() {
        return shotDelay;
    }
    public float getFireRate()  {
        return fireRate;
    }
    public float getReloadTimer()   {
        return reloadTimer;
    }
    public float getDamage()    {
        return damage;
    }
    public boolean isAutomatic()    {
        return automatic;
    }
    public Type getType()   {
        return type;
    }
}
