/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flocadev.zombieland;

/**
 *
 * @author nickm2422
 */

/*
 * Description:
 * Part of the pathfinding method.
 */
public class ExampleNode extends AbstractNode {

        public ExampleNode(int xPosition, int yPosition) {
            super(xPosition, yPosition, true);
        }

        public void sethCosts(AbstractNode endNode) {
            this.sethCosts((absolute(this.getxPosition() - endNode.getxPosition())
                    + absolute(this.getyPosition() - endNode.getyPosition()))
                    * BASICMOVEMENTCOST);
        }

        private int absolute(int a) {
            return a > 0 ? a : -a;
        }

}
