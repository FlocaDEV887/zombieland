/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flocadev.zombieland;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author flocc5441
 */

/*
 * The main drawing class; this class takes care of drawing the map as well as 
 * the player and zombies. Also takes care of the animations and UI, and basically
 * everything that has to do with graphics.
 */

public class MapRenderer {
    
    private MapLayout world;
    
    //Texture packs
    private TextureAtlas playerTex = new TextureAtlas("images/Player.pack");
    private TextureAtlas zombieTex = new TextureAtlas("images/Zombie.pack");
    private TextureAtlas groundTex = new TextureAtlas("images/Textures.pack");
    
    private TextureRegion obstacleImage, barricadeImage, groundTile;    //textures for the ground
    private TextureRegion sprite, zombieSprite, zombieAttackSprite, bulletSprite;
    private TextureRegion controlScreen, title, gameOverScreen; //Screens
    private TextureRegion colt1911, dual_colt1911, m16a1, ak47; //guns
    
    private Animation playerAnimation;
    private Animation zombieAnimation;
    
    private OrthographicCamera camera;      //Main camera that moves around the world
    private OrthographicCamera UIcamera;    //Used for the UI, including the status at the top-left
    
    private SpriteBatch batch;
    
    private ShapeRenderer shapeRenderer = new ShapeRenderer();
    
    private BitmapFont font = new BitmapFont(); //Text on the screen
    
    private float width, height;
    
    private Player player;
    
    private ArrayList<Zombie> zombies = new ArrayList<Zombie>();
    private ArrayList<Bullet> bullets = new ArrayList<Bullet>();
    private ArrayList<Tile> barricades = new ArrayList<Tile>();
    private ArrayList<Tile> obstacles = new ArrayList<Tile>();
    
    public static float ppuX;
    public static float ppuY;
    public static float camHeight;
    public static float camWidth;
            
    public MapRenderer(MapLayout map)   {
        world = map;
        
        //Setting images
        
            //Ground textures
        groundTile = groundTex.findRegion("GroundNormal");
        obstacleImage = groundTex.findRegion("ObstacleRock");
        barricadeImage = groundTex.findRegion("Barricade");
            //Character animations/images
        sprite = playerTex.findRegion("player");
        zombieSprite = zombieTex.findRegion("Zombie");
        zombieAttackSprite = zombieTex.findRegion("Zombie", 5);  
            //Screens
        controlScreen = new TextureRegion(new Texture("images/ControlScreen1.png"));
        title = new TextureRegion(new Texture("images/ZombieLand Title.png"));
        gameOverScreen = new TextureRegion(new Texture("images/youDied.png"));
            //Bullet
        bulletSprite = new TextureRegion(new Texture("images/bullet_sprite.png"));
            //For use in the status (gun images)
        colt1911 = new TextureRegion(new Texture("images/colt1911.png"));
        dual_colt1911 = new TextureRegion(new Texture("images/dual_colt1911.png"));
        m16a1 = new TextureRegion(new Texture("images/m16a1.png"));
        ak47 = new TextureRegion(new Texture("images/ak47.png"));
        
        //Get the required information from MapLayout
        player = world.getPlayer();
        zombies = world.getSpawningZombies();
        bullets = world.getCreatedBullets();
        obstacles = world.getCreatedObstacles();
        barricades = world.getCreatedBarricades();
        
        //Set up the SpriteBatch and viewing camera preferences
        batch = new SpriteBatch();
        camera = new OrthographicCamera(16, 12);
        camera.position.set(8f, 6f, 0);
        camera.update();
        camHeight = camera.viewportHeight;
        camWidth = camera.viewportWidth;
        //Set up the UI camera
        UIcamera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        UIcamera.position.set(Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2, 0);
        UIcamera.update();
        
        //Player animation
        TextureRegion[] playerFrames = new TextureRegion[4];
        for(int i = 0; i<4; i++)
        {
            playerFrames[i] = playerTex.findRegion("player", i+1);
        }
        
        //Zombie animation
        TextureRegion[] zombieFrames = new TextureRegion[4];
        for(int i = 0; i < 4; i++)
        {
            zombieFrames[i] = zombieTex.findRegion("Zombie", i + 1);
        }
        
        playerAnimation = new Animation(1/8f, playerFrames);
        zombieAnimation = new Animation(1/8f, zombieFrames);
        
        Gdx.input.setInputProcessor(player);
    }
    
    //Sets the size of the screen and scales everything else
    public void setSize(float width, float height)  {
        this.width = width;
        this.height = height;
        ppuX = this.width/camera.viewportWidth;
        ppuY = this.height/camera.viewportHeight;
    }
    
    
    //Renders the main game
    public void render()    {
        
        /* Camera goes not until the very edge of the screen, but one tile from
         * the screen; this one-tile layer around the screen is where zombies 
         * spawn randomly from, and the player cannot go on these tiles, thus 
         * they cannot be seen.
         */
        
        //Move camera horizontally for the player's x position
        if (player.getX() < (camera.viewportWidth/2 + 1))
            camera.position.x = camera.viewportWidth/2 + 1;
        else if (player.getX() > (world.getWidth() - 1) - camera.viewportWidth/2)
            camera.position.x = (world.getWidth() - 1) - camera.viewportWidth/2;
        else
            camera.position.x = player.getX();
        
        //Move camera vertically for the player's y position
        if (player.getY() < (camera.viewportHeight/2 + 1))
            camera.position.y = camera.viewportHeight/2 + 1;
        else if (player.getY() > (world.getHeight() - 1) - camera.viewportHeight/2)
            camera.position.y = (world.getHeight() - 1) - camera.viewportHeight/2;
        else
            camera.position.y = player.getY();
        
        //Be sure to update the viewing position
        player.updateCamPos(camera.position.x-camera.viewportWidth/2, camera.position.y-camera.viewportHeight/2);
        camera.update();
        
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        //Drawing commences
        
        try {   //For debugging/exception handling
            //Drawing tiles from MapLayout
            int startX = (int)(Math.floor(camera.position.x - camera.viewportWidth/2));
            int endX = (int)(Math.ceil(camera.position.x + camera.viewportWidth/2));
            int startY = (int)(Math.floor(camera.position.y - camera.viewportHeight/2));
            int endY = (int)(Math.ceil(camera.position.y + camera.viewportHeight/2));
            for (int i = startX; i < endX; i++)    {
                for (int j = startY; j < endY; j++) {
                    batch.draw(groundTile, i, j, Tile.SIZE, Tile.SIZE);
                }
            }
        }catch(NullPointerException e) {
            System.out.println(e);
        }
        
        // draws all the obstacles
        Iterator<Tile> iObstacles = obstacles.iterator();
            while (iObstacles.hasNext()) {
                Obstacle currentObstacle =  (Obstacle) iObstacles.next();
                batch.draw(obstacleImage, currentObstacle.x, currentObstacle.y, Tile.SIZE, Tile.SIZE);
        }
        
        // draws all the barricades
        Iterator<Tile> iBarricades = barricades.iterator();
            while (iBarricades.hasNext()) {
                Barricade currentBarricade =  (Barricade) iBarricades.next();
                batch.draw(barricadeImage, currentBarricade.getX(), currentBarricade.getY(), Tile.SIZE, Tile.SIZE);
        }
        
        // draws the player and the player animation
        if(player.getXVel() == 0 && player.getYVel() == 0)
             {
                 batch.draw(sprite, player.getX(), player.getY(), Player.SIZE/2, Player.SIZE/2, Player.SIZE, Player.SIZE, 1,1, player.getDirection());
             }else
        {
            //draw the player if he is running
            batch.draw(playerAnimation.getKeyFrame(player.getStateTime(), true), player.getX(), player.getY(), Player.SIZE/2, Player.SIZE/2, Player.SIZE, Player.SIZE, 1,1, player.getDirection());
        }
        
        // draws the zombies and their animations for attacking and walking
        Iterator<Zombie> iZombie = zombies.iterator();
        while (iZombie.hasNext())   {
            Zombie currentZombie = iZombie.next();
            if(currentZombie.getXVel() == 0 && currentZombie.getYVel() == 0)
            {
                batch.draw(zombieSprite, currentZombie.getX(), currentZombie.getY(), Zombie.SIZE/2, Zombie.SIZE/2, Zombie.SIZE, Zombie.SIZE, 1,1, currentZombie.getDirection());
            }else if(currentZombie.isHitByPlayer(player) == false)
                {
                    //draw the zombie if he is running
                    batch.draw(zombieAnimation.getKeyFrame(currentZombie.getStateTime(), true), currentZombie.getX(), currentZombie.getY(), Zombie.SIZE/2, Zombie.SIZE/2, Zombie.SIZE, Zombie.SIZE, 1,1, currentZombie.getDirection());
                }else if(currentZombie.isHitByPlayer(player) == true && currentZombie.getattackDelay() <= 0.25f)
                   //if the zombie is attacking the player
                    {
                        batch.draw(zombieAttackSprite, currentZombie.getX(), currentZombie.getY(), Zombie.SIZE/2, Zombie.SIZE/2, Zombie.SIZE, Zombie.SIZE, 1,1, currentZombie.getDirection());
                    }else
                        {
                            batch.draw(zombieSprite, currentZombie.getX(), currentZombie.getY(), Zombie.SIZE/2, Zombie.SIZE/2, Zombie.SIZE, Zombie.SIZE, 1,1, currentZombie.getDirection());
                        }
        }
        
        //bullet
        Iterator<Bullet> iBullet = bullets.iterator();
        shapeRenderer.setColor(Color.RED);
        while(iBullet.hasNext())    {
            Bullet currentBullet = iBullet.next();
            batch.draw(bulletSprite, currentBullet.getX(), currentBullet.getY(), Bullet.SIZE, Bullet.SIZE);
        }
        
        //Except for shapes:
        camera.update();
        shapeRenderer.setProjectionMatrix(UIcamera.combined);
        
        //Drawing concludes
        batch.end();
        
        //Drawing shapes commences
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled); //Begin drawing polygons, shapes, etc.        
        
        //Status

        //Health bar
        shapeRenderer.setColor(Color.RED);  //Background
        shapeRenderer.rect(30, 425, 220, 32);
        shapeRenderer.setColor(Color.GREEN);    //Actual amount of health
        shapeRenderer.rect(30, 425, 220*((float)player.getHealth()/100), 32);
        
        //Drawing shapes concludes
        shapeRenderer.end();
        
        //Drawing commences (only for barricades' health
        shapeRenderer.setProjectionMatrix(camera.combined);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        
        // draws the barricades' health bar
        Iterator<Tile> iBarricades2 = barricades.iterator();
            while (iBarricades2.hasNext()) {
                Barricade currentBarricade =  (Barricade) iBarricades2.next();
                shapeRenderer.setColor(Color.RED);  //Background
                shapeRenderer.rect(currentBarricade.getX() + 0.1f, currentBarricade.getY() + 1.05f, 0.8f, 0.1f);
                shapeRenderer.setColor(Color.GREEN);    //Actual amount of health
                shapeRenderer.rect(currentBarricade.getX() + 0.1f, currentBarricade.getY() + 1.05f, 0.8f*((float)currentBarricade.getHealth()/100), 0.1f);    
            }
            
        // drawing concludes
        shapeRenderer.end();
        
        //More UI drawing
        batch.setProjectionMatrix(UIcamera.combined);
        batch.begin();
        
        //Text on health bar
        font.setColor(Color.BLACK);
        font.setScale(2);
        font.draw(batch, "HEALTH", 30, 455); //y position
        
        //"enter to pause" text
        font.setColor(Color.WHITE);
        font.setScale(1.1f);
        font.draw(batch, "Press ENTER to pause", 600, 20);
        
        // draws the gun type, bullets in the clip and clip size
        font.setColor(Color.BLACK);
        font.setScale(1.75f);
        if (player.getGun().getType() == Gun.Type.Colt1911) {   //draw colt 1911 image
            font.draw(batch, "Colt 1911", 30, 420);
            batch.draw(colt1911, 170, 395);
            font.draw(batch, (int)player.getGun().getBulletsInClip() + "/" + (int)player.getGun().getClipSize(), 215, 420);
        }
        else if (player.getGun().getType() == Gun.Type.Dual_Colt1911)   {   //draw dual colt 1911 image
            font.draw(batch, "Dual Colt 1911", 30, 420);
            batch.draw(dual_colt1911, 205, 395);
            font.draw(batch, (int)player.getGun().getBulletsInClip() + "/" + (int)player.getGun().getClipSize(), 255, 420);
        }
        else if (player.getGun().getType() == Gun.Type.M16A1)   {   //draw m16a1 image
            font.draw(batch, "M16A1", 30, 420);
            batch.draw(m16a1, 150, 395);
            font.draw(batch, (int)player.getGun().getBulletsInClip() + "/" + (int)player.getGun().getClipSize(), 215, 420);
        }
        else if (player.getGun().getType() == Gun.Type.AK47)    {   //draw ak47 image
            font.draw(batch, "AK47", 30, 420);
            batch.draw(ak47, 150, 395);
            font.draw(batch, (int)player.getGun().getBulletsInClip() + "/" + (int)player.getGun().getClipSize(), 215, 420);
        }
        
        // draws the reloading sign if the player is reloading
        if(player.getGun().getReloadTimer() > 0)
        {
            font.setColor(Color.RED);
            font.draw(batch, "Reloading " + (int)(player.getGun().getReloadTimer()/60 + 1) , 215, 395);
            font.setColor(Color.BLACK);
        }
        
        // draws the amount of points the player has
        font.draw(batch, "Points: " + player.getScore(), 30, 390);
        
        // draws the wave the player is on
        font.setScale(2);
        font.draw(batch, "WAVE " + world.getWaveAsNumeral(), 30, 360);
        
        // draws the sign to the player that an upgrade is possible
        if (world.getUpgrade()) {
            font.setColor(Color.CYAN);
            font.setScale(2);
            font.draw(batch, "Press SHIFT to upgrade gun (cost: " + world.upgradeCost() + ")", 30, 40);
        }
        
        batch.end();
    }
    
    
    //main menu
    public void renderMenu()    {
        //set the cameras
        batch.setProjectionMatrix(UIcamera.combined);
        shapeRenderer.setProjectionMatrix(UIcamera.combined);
        
        //Drawing commences
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled); //Begin drawing polygons, shapes, etc.
        
        shapeRenderer.setColor(0.5f, 0.1f, 0.1f, 1f);  //Background
        shapeRenderer.rect(0,0,width,height);
        
        shapeRenderer.setColor(0.2f, 0.8f, 0.15f, 1f);  //Start Game button
        shapeRenderer.rect(width/2-80,height/2-35,180,50);
        
        shapeRenderer.setColor(0.2f, 0.8f, 0.15f, 1f);  //Control button
        shapeRenderer.rect(width/2-65,height/2-95,150,50);
        
        shapeRenderer.setColor(0.2f, 0.8f, 0.15f, 1f);  //Tutorial button
        shapeRenderer.rect(width/2-65,height/2-155,150,50);
        
        shapeRenderer.end();
        
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line); //Begin drawing outlines
        
        shapeRenderer.setColor(Color.BLACK);  //Start Game button
        shapeRenderer.rect(width/2-80,height/2-35,180,50);
        
        shapeRenderer.setColor(Color.BLACK);  //Control button
        shapeRenderer.rect(width/2-65,height/2-95,150,50);
        
        shapeRenderer.setColor(Color.BLACK);  //Tutorial button
        shapeRenderer.rect(width/2-65,height/2-155,150,50);
        //Drawing concludes
        shapeRenderer.end();
        
        //To draw strings only
        batch.begin();
        
        batch.draw(title, width/15, 3*height/5, 7*width/8, height/6);
        //Text on start game button
        font.setColor(Color.BLACK);
        font.setScale(2);
        font.draw(batch, "Start Game", width/2-60, height/2+5); 
        
        //Text on control button
        font.setScale(1.75f);
        font.draw(batch, "Controls", width/2-38, height/2-60);
        
        //Text on tutorial button
        font.setScale(1.75f);
        font.draw(batch, "Instructions", width/2-55, height/2-120);
        
        batch.end();

    }
    
    //control screen
    public void renderControlMenu()    {
        //set the cameras
        batch.setProjectionMatrix(UIcamera.combined);
        shapeRenderer.setProjectionMatrix(UIcamera.combined);
        
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled); //Begin drawing polygons, shapes, etc.        
        
        shapeRenderer.setColor(0.5f, 0.1f, 0.1f, 1f);  //Background
        shapeRenderer.rect(0,0,width,height);
        
        shapeRenderer.setColor(0.2f, 0.8f, 0.15f, 1f);  //Main menu button
        shapeRenderer.rect(3*width/4, height/10 -35,180,50);
        
        //Drawing concludes
        shapeRenderer.end();
        
        batch.begin();
        
        batch.draw(controlScreen, 0, 0); // control screen image
        
        //Text on start game button
        font.setColor(Color.BLACK);
        font.setScale(2);
        font.draw(batch, "Main Menu", 3*width/4+10, height/10); 
        
        batch.end();
    }
    

    //instruction screen
    public void renderTutorial()    {
        //set the cameras
        batch.setProjectionMatrix(UIcamera.combined);
        shapeRenderer.setProjectionMatrix(UIcamera.combined);
        
        //Drawing commences
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled); //Begin drawing polygons, shapes, etc.        
        
        shapeRenderer.setColor(0.5f, 0.1f, 0.1f, 1f);  //Background
        shapeRenderer.rect(0,0,width,height);
        
        shapeRenderer.setColor(0.2f, 0.8f, 0.15f, 1f);  //Start Game button
        shapeRenderer.rect(width/2-80,height/2-35,180,50);
        
        //Drawing concludes
        shapeRenderer.end();
        
        //To draw strings only
        batch.begin();
        
        //Text on start game button
        font.setColor(Color.BLACK);
        font.setScale(2);
        font.draw(batch, "Main Menu", width/2-60, height/2+5); 
        
        //instructions
        font.setColor(Color.BLACK);
        font.setScale(1.3f);
        font.draw(batch, "You must survive.", width/2 - 60, 9*height/10 - 30); 
        font.draw(batch, "Zombies are spawning outside of your base.", width/2 - 170, 9*height/10 - 60); 
        font.draw(batch, "Once they've broken through the barricades, you will need to fight them off.", width/2-280, 9*height/10 - 90); 
        font.draw(batch, "Score points by killing zombies.", width/2-120, height/2 - 90); 
        font.draw(batch, "Points can be used to upgrade your gun later in the game.", width/2-230, height/2 - 120); 
        font.draw(batch, "Good luck.", width/2-30, height/2 - 170); 
        
        batch.end();
        }
    
    //pause menu
    public void renderPauseMenu()    {
        
        //renders main game in the background
        render();
        
        //set up the cameras
        batch.setProjectionMatrix(UIcamera.combined);
        shapeRenderer.setProjectionMatrix(UIcamera.combined);
        
        //enable translucent colours
        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        
        //Drawing commences
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled); //Begin drawing polygons, shapes, etc.        
        
        shapeRenderer.setColor(0.1f, 0.1f, 0.1f, 0.8f); //screen to shade the game screen border
        shapeRenderer.rect(0,0,width,height);
        
        shapeRenderer.setColor(0.5f, 0.1f, 0.1f, 1f);  //Background of menu
        shapeRenderer.rect(width/2-100,height/2-110,220,140);
        
        shapeRenderer.setColor(0.2f, 0.8f, 0.15f, 1f);  //Resume Game button
        shapeRenderer.rect(width/2-65,height/2-35,150,50);
        
        shapeRenderer.setColor(0.2f, 0.8f, 0.15f, 1f);  //Main menu button
        shapeRenderer.rect(width/2-65,height/2-95,150,50);
        
        //Drawing concludes
        shapeRenderer.end();
        Gdx.gl.glDisable(GL20.GL_BLEND);
        
        batch.begin();
        
        //Text on start game button
        font.setColor(Color.BLACK);
        font.setScale(1.2f);
        font.draw(batch, "Resume", width/2-25, height/2+10); 
        font.draw(batch, "(Press Enter)", width/2-40, height/2-10); 
        
        //Text on main menu button
        font.setScale(1.75f);
        font.draw(batch, "Main Menu", width/2-52, height/2-60);
        
        batch.end();

    }
    
    //gameover screen
    public void renderGameOver()    {
        //set the camera
        batch.setProjectionMatrix(UIcamera.combined);
        shapeRenderer.setProjectionMatrix(UIcamera.combined);
        
        //"you died" screen
        batch.begin();
        batch.draw(gameOverScreen, 0, 0, width, height);
        batch.end();
        
        //Drawing commences
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled); //Begin drawing polygons, shapes, etc.        
        
        //Main menu button
        shapeRenderer.setColor(0.2f, 0.8f, 0.15f, 1f);  
        shapeRenderer.rect(3*width/4, height/10 -35,180,50);
        
        //Drawing concludes
        shapeRenderer.end();
        
        //To draw strings only
        batch.begin();
        
        //Text on main menu button
        font.setColor(Color.BLACK);
        font.setScale(2);
        font.draw(batch, "Main Menu", 3*width/4+10, height/10); 
        
        //score
        font.setColor(0.7f, 0.3f, 0.3f, 1f);
        font.setScale(2);
        font.draw(batch, "Score: " + player.getScore(), width/2-60, height/4); 
        
        batch.end();
        }
}
