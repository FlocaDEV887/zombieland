/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flocadev.zombieland;

/**
 *
 * @author flocc5441
 */

/*
 * The master class of obstacles and barricades; the world is composed of these tiles, 
 * which can be either null tiles (floor), obstacles, or barricades.
 */

public abstract class Tile {
    
    public static final float SIZE = 1f;
    protected float x, y;
    
    //To tell the MapRenderer what to draw based on whether the tile is null;
    //else !=null, whether its type is of obstacle or barricade
    public enum Type    {
        OBSTACLE, BARRICADE
    }
    
    private Type type;
    
    public Tile(float initX, float initY, Type type)   {
        this.x = initX;
        this.y = initY;
        this.type = type;
    }
    
    // gets whether the tile is an obstacle, barricade or null (ground)
    public Type getType()   {
        return type;
    }
}
