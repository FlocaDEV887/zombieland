/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flocadev.zombieland;

import com.badlogic.gdx.math.Rectangle;
import static com.flocadev.zombieland.Tile.SIZE;

/**
 *
 * @author flocc5441
 */

/*
 * A subclass of Tile; the player and zombies cannot go through this tile (i.e.
 * collisions exist) while it is in a default or repairing/breaking state; when it is
 * broken, players and zombies can pass through the barricade. If a zombie is 
 * outside the main building and his nearest entrance is an unbroken barricade,
 * he will first go there to break it. Barricades can be shot through.
 */

public class Barricade extends Obstacle {
    
    private double health;
    private boolean dead;   //Basically if the barricade is destroyed
    
    public enum State   {
        DEFAULT, BROKEN, BREAKING, REPAIRING
    }
    
    private State state;
    
    public Barricade(float x, float y)
    {
        super(x, y, Type.BARRICADE);
        state = State.DEFAULT;  //Start off at a default state of full health
        health = 100;   //100 is max health
        dead = false;
    }
    
    public void update()    {
        //If there is no health left and the player is not repairing that barricade
        if (health <= 0 && state != State.REPAIRING)
            state = State.BROKEN;
        //Once max health has been reached, it must stay at 100, and be at default position
        else if (health >= 100) {
            health = 100;
            state = State.DEFAULT;
        }
        
        while (state == State.REPAIRING)    {
            health += 5;
            
            //Delay for one second (only restore 100hp per second)
            try {
                Thread.sleep(1000);
            }catch(InterruptedException e)  {
                Thread.currentThread().interrupt();
            }
            
            //This exits the loop; once max health is reached, the barricade is 
            //done being repaired
            if (health >= 100)  {
                health = 100;
                state = State.DEFAULT;
            }
        }
    }
    
    //Player calls this method when repairing the barricades
    public void repair()    {
        state = State.REPAIRING;
    }
    
    //For zombies and players to know how to interact with the barricade.
    //Also to render the correct images (A broken barricade looks different than
    //a default one, or one being repaired)
    public State getState()    {
        return state;
    }
    
    public void setState(State s)   {
        state = s;
    }
    
    public double getHealth()
    {
        return health;
    }
    
    //Used when zombies hit barricade
    public void subtractHealth(double damage)
    {
        if(health > 0)
        {
            health -= damage;
        }
    }
    
    //Used when player repairs barricade
    public void addHealth(double healthGained)
    {
        health += healthGained;
    }
    
    public float getX()
    {
        return x;
    }
    
    public float getY()
    {
        return y;
    }
    
    // returns if barricade is dead or not
    public boolean getIsDead()
    {
        return dead;
    }
    
    //Collision rectangle
    public Rectangle getRect(){
        Rectangle rect;
        rect = new Rectangle(x, y, SIZE, SIZE);
        return(rect);
    }
}
