package com.flocadev.zombieland;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import java.io.File;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

/*
 * "ZombieLand", by FlocaDEV (created by Catalin Floca, Matt Nickel, Matthew Richards)
 * Main Class
 * Description: Handles screens and main soundtrack. Delta time begins here.
 */

public class MainGame extends Game {
    
    //Create screen, controller, world layout, and renderer
    private Screen screen;
    private MapLayout world;
    private MapRenderer renderer;
    private WorldController control;
    private NodeBuilder nodeBuilder;
    
    //Soundtracks (title screen and the main game's soundtrack)
    private Clip title, main;
    private File titleSong, mainSong;
    
    @Override
    public void create () {
        //Create the world
        setScreen(screen);
        world = new MapLayout();
        renderer = new MapRenderer(world);
        control = new WorldController(world, world.getWidth(), world.getHeight(), nodeBuilder);
        
        //Point to the file locations of the sound
        titleSong = new File("sounds/title_screen_track.wav");
        mainSong = new File("sounds/main_soundtrack.wav");
        
        //Open sound tracks
        try {
            //Main menu sound track
            AudioInputStream AIS_Song = AudioSystem.getAudioInputStream(titleSong);
            title = AudioSystem.getClip();
            title.open(AIS_Song);
        }catch(Exception e) {
            e.printStackTrace();
        }
        try {
            //Main game sound track
            AudioInputStream AIS_Song = AudioSystem.getAudioInputStream(mainSong);
            main = AudioSystem.getClip();
            main.open(AIS_Song);
        }catch(Exception e) {
            e.printStackTrace();
        }
        
        //Set the WorldController to handle input and and other controls, including the player
        InputMultiplexer inputMultiplexer = new InputMultiplexer();
        inputMultiplexer.addProcessor(control);
        inputMultiplexer.addProcessor(world.getPlayer());
        Gdx.input.setInputProcessor(inputMultiplexer);
    }

    @Override
    public void resize (int width, int height)  //Resize the window
    {
        renderer.setSize(width, height);
    }
    
    @Override
    public void render () {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        //This handles what screen to render; the main game screen and the title
        //screen also handle the music/soundtrack being played
        if(control.getLevel() == 0)
        {
            //Control soundtrack
            main.stop();
            if (!title.isActive())
                title.loop(Clip.LOOP_CONTINUOUSLY);
            
            renderer.renderMenu();
        }
        if(control.getLevel() == 1)
        {
            renderer.renderControlMenu();
        }
        if(control.getLevel() == 2)
        {
            renderer.renderTutorial();
        }
        if(control.getLevel() == 3)
        {
            //Control soundtrack
            title.stop();
            if (!main.isActive())
                main.loop(Clip.LOOP_CONTINUOUSLY);
            
            renderer.render();  //show the world
            control.update(Gdx.graphics.getDeltaTime());
        }
        
        if(control.getLevel() == 4)
        {
            renderer.renderPauseMenu();
        }
        if(control.getLevel() == 5)
        {
            renderer.renderGameOver();
        }
    }
}
