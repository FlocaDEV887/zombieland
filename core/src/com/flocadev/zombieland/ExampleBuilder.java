/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flocadev.zombieland;

/**
 *
 * @author nickm2422
 */

/*
 * Description:
 * Part of the pathfinding method. Creates the nodes.
 */
public class ExampleBuilder implements NodeBuilder {

        @Override
        public AbstractNode createNode(int x, int y) {
            return new ExampleNode(x, y);
        }

}
