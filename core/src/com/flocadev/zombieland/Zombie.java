/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flocadev.zombieland;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 *
 * @author flocc5441
 */

/*
 * The enemy class; this is used in an iterator. This class handles everything
 * that has to do with the zombie and his pursuit to attack the player. This class
 * includes setting zombie states and goals according to their state, zombie vertical
 * and horizontal movements, zombie 360 rotation, zombie attacking the player, and
 * zombie interactions with barricades, obstacles and the player 
 */

public class Zombie {
    public static final float SIZE = 1f;
    public static final float SPEED = 3.5f;
    
    private Rectangle bounds;
    private Vector2 position, velocity;
    private int direction;
    private int label;
    private float stateTime = 0f;
    private float attackTimer = 0;
    private float attackDelay = 0;
    private float health = 100f;
    private float revivingHealth = 1f;
    private float reviveDelay = 100f;
    
    private boolean hasCollided = false;    //Only for use outside the barricades

    public State getState() {
        return state;
    }
    
    public enum State {
        OUTSIDE, INSIDE, BREAKING   //Outside the barrier, inside, and destroying it
    }
    
    private State state;
    private Player player;
        
    public Zombie(float x, float y, Player p) {
        bounds = new Rectangle (x, y, SIZE, SIZE);
        position = new Vector2(x, y);
        velocity = new Vector2(0, 0);
        player = p;
    }
    
    // updates the specific zombie's state and position based on it's recent movements 
    public void update(float delta)    {
        stateTime += delta;
        if(attackTimer <=2)
        {
            attackDelay += delta;
            attackTimer += delta;
        }
        
        // determines if the zombie's state is outside by looking 
        // at position and if it is not already breaking or inside
        if(((position.y >= 0 && position.y < 4)   ||
           (position.y <= 34 && position.y > 30) ||
           (position.x >= 0 && position.x < 4)   ||
           ((position.x > 25 && position.x <= 32) && (position.y > 11 || position.y < 4))) &&
            state != State.BREAKING && state != State.INSIDE)
            {
                state = State.OUTSIDE;
            }
        
        // determines if the zombies's state is inside
        // based on the zombie's position and if it is not breaking
        if(((position.x >= 3 && position.x <= 24 && 
              position.y >= 3 && position.y < 30) || 
              (position.x > 24 && position.x < 31 &&
              position.y < 10 && position.y >= 5)) &&
              state != State.BREAKING)
            {
                state = State.INSIDE;
            }
        
        //Act according to the state of the zombie
        if (state == State.OUTSIDE)
            moveToBarricade(delta);
        else if (state == State.BREAKING)
            destroyBarricade();
        else if (state == State.INSIDE)
            moveToPlayer(delta);
        
        //Revive zombie health
        if (reviveDelay >= 100 && health < 100)  {
            health += revivingHealth;
            reviveDelay = 0;
        }
        reviveDelay += 1;
        
    }
    
    // updates the zombie position when moving to the player in the zombie inside state
    public void moveToPlayer(float delta)  {
        //Just updates position
        position.add(velocity.x*delta, velocity.y*delta);
    }
    
    public void moveToBarricade(float delta)   {
        /**
         * The coordinates where the zombie spawns will determine which barricade
         * the zombie moves towards (barricade numbers marked on paper). The spawn
         * locations are identified and numbered as "zones", also done on paper.
         * Calculations have been made to determine which barricade the zombie will
         * move towards, based on distance and size of zones.
         */
        
        //Zone 1 (move to barricade 1):
        //Spawning underneath barricade
        if ((position.x >= 4 && position.x <= 32) && position.y < 4)   {
            //Vertical movement
            if (position.y < 3 && (position.x >= 9 && position.x <= 10)) {
                velocity.y = SPEED;
            }
            else if (position.y < 2 && (position.x < 9 || position.x > 10)) {
                velocity.y = SPEED;
            }
            else {  //if position.y == 2
                velocity.y = 0;
            }

            //Horizontal movement
            if (position.x < 9)
                velocity.x = SPEED;
            else if (position.x > 10)
                velocity.x = -SPEED;
            else
                velocity.x = 0;
        }
        //Spawning to the left of barricade
        if ((position.x >= 0 && position.x < 4) && (position.y >= 0 && position.y < 10))    {
            //Vertical movement
            if (position.y > 2)
                velocity.y = -SPEED;
            else if (position.y < 2)
                velocity.y = SPEED;
            else
                velocity.y = 0;

            //Horizontal movement
            if (position.x < 2 && position.y > 2)
                velocity.x = SPEED;
            else if (position.y <= 2)    //x can be any value here
                velocity.x = SPEED;
            else
                velocity.x = 0;
        }

        //Zone 2 (move to barricade 2)
        //Only spawn position is to the left of barricade
        if ((position.y >= 10 && position.y < 29) && position.x < 4)   {
            //Horizontal movement
            if (position.x < 3 && (position.y >= 20 && position.y <= 21)) {
                velocity.x = SPEED;
            }
            else if (position.x < 2 && (position.y < 20 || position.y > 21)) {
                velocity.x = SPEED;
            }
            else {
                velocity.x = 0;
            }
            
            //Vertical movement
            if (position.y < 20)
                velocity.y = SPEED;
            else if (position.y > 21)
                velocity.y = -SPEED;
            else
                velocity.y = 0;
        }
        
        //Zone 3 (move to barricade 3)
        //Spawn to left of barricade
        if (position.x < 4 && (position.y >= 29 && position.y <= 34))  {
            //Vertical movement
            if (position.y < 32)
                velocity.y = SPEED;
            else if (position.y > 32)
                velocity.y = -SPEED;
            else
                velocity.y = 0;

            //Horizontal movement
            if (position.x < 2 && position.y < 32)
                velocity.x = SPEED;
            else if (position.y >= 32)
                velocity.x = SPEED;
            else
                velocity.x = 0;
        }
        //Spawn above the barricade
        if ((position.x > 3 && position.x <= 19) && position.y > 31)    {
            //Vertical movement
            if (position.y > 31 && (position.x >= 12 && position.x <= 13)) {
                velocity.y = -SPEED;
            }
            else if (position.y > 32 && (position.x < 12 || position.x > 13)) {
                velocity.y = -SPEED;
            }
            else {
                velocity.y = 0;
            }
            
            //Horizontal movement
            if (position.x < 12)
                velocity.x = SPEED;
            else if (position.x > 13)
                velocity.x = -SPEED;
            else
                velocity.x = 0;
        }
        
        //Zone 4 (move to barricade 4)
        //Spawn above barricade
        if ((position.x > 19 && position.x < 26) && (position.y <= 34 && position.y > 30))  {
            //Vertical movement
            if (position.y > 32)
                velocity.y = -SPEED;
            else
                velocity.y = 0;

            //Horizontal movement
            if (position.x < 27)
                velocity.x = SPEED;
            else
                velocity.x = 0;
        }
        //Spawn to the right of barricade
        if (position.x >= 26 && position.y >= 23)   {
            //Avoid walking alongside the wall
            if (position.x < 27 && (position.y >= 30 || position.y <= 27))    {
                velocity.x = SPEED;
                velocity.y = 0;
            }
            else    {
                //Horizontal movement
                if (position.x >= 26 && (position.y >= 28 && position.y <= 29)) {
                    velocity.x = -SPEED;
                }
                else if (position.x >= 27 && (position.y < 28 || position.y > 29)) {
                    velocity.x = -SPEED;
                }
                else {
                    velocity.x = 0;
                }

                //Vertical movement
                if (position.y > 29)
                    velocity.y = -SPEED;
                else if (position.y < 28)
                    velocity.y = SPEED;
                else
                    velocity.y = 0;
            }
        }
        
        //Zone 5 (move to barricade 5)
        //Can only spawn to right of barricade
        if (position.x >= 26 && (position.y > 12 && position.y < 23))  {
            //Horizontal movement
            if (position.x > 26 && (position.y >= 15 && position.y <= 16)) {
                velocity.x = -SPEED;
            }
            else if (position.x > 27 && (position.y < 15 || position.y > 16)) {
                velocity.x = -SPEED;
            }
            else {
                velocity.x = 0;
            }
            
            //Vertical movement
            if (position.y < 15)
                velocity.y = SPEED;
            else if (position.y > 16)
                velocity.y = -SPEED;
            else
                velocity.y = 0;
        }

        position.add(velocity.x*delta, velocity.y*delta);
    }
    
    //determines the direction the zombie is facing based on where the player is
    public int getDirection()
    {
        if(position.x < player.getX() + SIZE/2) //if the zombie is to the left of the player (Quadrant 2 and 3)
        {
            direction = (int)(180 - (Math.atan((position.y-player.getY()-SIZE/2)/(player.getX()+SIZE/2-position.x)))*180/Math.PI);
        
        }else if (position.y < player.getY() + SIZE/2 && position.x > player.getX() + SIZE/2) //if the zombie is below and to the right of the player 
        {
            direction = (int)(360 + (Math.atan((position.y-(player.getY()+SIZE/2))/(position.x-(player.getX()+SIZE/2))))*180/Math.PI);
        
        }else if (position.x > player.getX() + SIZE/2 && position.y > player.getY() + SIZE/2) //if the zombie is above and to the right of the player 
        {
            direction = (int)((Math.atan((position.y-(player.getY()+SIZE/2))/(position.x-(player.getX()+SIZE/2))))*180/Math.PI);
        }
        
        return direction + 180;
    }
    
    // if the zombie's state is breaking then the zombie will destroy the barricade
    public void destroyBarricade()   {
        // dont allow the zombie to move
        velocity.x = 0;
        velocity.y = 0;
        
        position.add(velocity.x, velocity.y);
    }
    
    // Setters and Getters:
    
    public void setState(State s)   {
        state = s;
    }
    
    public float getStateTime()
    {
        return stateTime;
    }
    
    public float getX()
    {
        return position.x;
    }
    
    public float getY()
    {
        return position.y;
    }
    
    public void setX(float x)
    {
        position.x = x;
    }
    
    public void setY(float y)
    {
        position.y = y;
    }
    
    public float getXVel()
    {
        return velocity.x;
    }
    
    public float getYVel()
    {
        return velocity.y;
    }
    
    public float getattackDelay()
    {
        return attackDelay;
    }

    public float getHealth()    {
        return health;
    }
    
    public void setXVel(float v)
    {
        velocity.x = v;
    }
    
    public void setYVel(float v)
    {
        velocity.y = v;
    }
    
    public void setLabel(int i) {
        label = i;
    }
    
    public void setColl(boolean b)  {
        hasCollided = b;
    }
    
    public void hit(float h)   {
        health -= h;
    }
    
    //Collision rectangle
    public Rectangle getRect(){
        Rectangle rect;
        rect = new Rectangle(position.x, position.y, SIZE, SIZE);
        return(rect);
    }
    
    // determines if the zombie and player collide
    public boolean isHitByPlayer(Player p){
        Rectangle objRect;
        Rectangle obj2Rect;
        boolean collision;

        // get the rectangle for the current zombie
        objRect = this.getRect();
        
        // get the rectangle for the player
        obj2Rect = p.getRect();
        
        // determines if the zombie and player collide/overlap
        collision = objRect.overlaps( obj2Rect );

        return(collision);
    }
    
    // determines when the zombie can attack the player 
    // and with the damage the zombie deals to the player
    public void attackPlayer()
    {
        if(attackTimer >= 1)
        {
            player.subtractHealth(20);
            attackTimer = 0;
            attackDelay = 0;
        }
    }
    
    // determines if a bullet and zombie collide
    public boolean isHitByBullet(Bullet b){
        Rectangle objRect;
        Rectangle obj2Rect;
        boolean collision;

        // get the rectangle for the current zombie
        objRect = this.getRect();
        
        // get the rectangle for the bullet object
        obj2Rect = b.getRect();
        
        // determine if the zombie and bullet collide/overlap
        collision = objRect.overlaps( obj2Rect );

        return(collision);
    }
    
    // determines if the zombie is touching the barricade
    public boolean isTouchingBarricade(Barricade b)
    {
        boolean collision = false;
        
        // get the barricade's x and y and subtract it from the zombie's x and y
        float nearX = getX() - b.getX();
        float nearY = getY() - b.getY();
        
        // essentially if the barricade and the zombie are touching eachother there is a collision
        if(nearX <= 1 && nearX >= -1 && nearY <= 1 && nearY >= -1)
        {
            collision = true;
        }
        
        return(collision);
    }

}
