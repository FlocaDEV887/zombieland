/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flocadev.zombieland;

/**
 *
 * @author nickm2422
 */
/**
 *
 * @author nickm2422
 
 /*
 * Description:
 * AbstractNode is the main class that does the "behind the scenes" work for the pathfinding. 
 * Basically, this is how the pathfinding works:
 * There are two main points on the map: the start position and the end position (the goal)
 * The object is to find a path (and ultimately take that path) that leads from he start to the goal.
 * First of all, the map is divided into tiles, called nodes.
 * The next thing to be done is eliminate all of the tiles on the map where the character cannot go
 * Each tile has a boolean called "walkable" that is either true or false. 
 * Next, there are 3 different "costs"(or distances) that are calculated from the nodes: gCost, hCost, and fCost
 * The gCost is the distance from the current node to the start node
 * The hCost is the distance from the current node to the goal node
 * The fCost is the distance from the start node going through the current to the goal node.
 * Essentially, the program will go through the nodes and determine which nodes will create the shortest path
 * from the start node to the goal node. 
 * Finally, using the path (which is essentially a list of nodes), the character at the start node will check 
 * where his position is relative to the next position in the path, and move towards that next node.
 */
public abstract class AbstractNode {
    
    private int xPosition, yPosition;
    private boolean walkable, diagonally;
    private AbstractNode previous;
    private int gCosts, hCosts;
    
    // costs to move sideways from one node(tile) to another
    protected static final int BASICMOVEMENTCOST = 10;
    // costs to move diagonally from one node(tile) to another
    protected static final int DIAGONALMOVEMENTCOST = 14;

    
    public AbstractNode(int xPosition, int yPosition, boolean walkable) {
        this.xPosition = xPosition;
        this.yPosition = yPosition;
        this.walkable = walkable;
    }
    
    //if the next node is diagonal from the current
    public boolean isDiagonaly() {
        return diagonally;
    }
    
    public void setIsDiagonaly(boolean isDiagonaly) {
        this.diagonally = isDiagonaly;
    }
    
    //gets x position of node
    public int getxPosition() {
        return xPosition;
    }
    
    //gets y position of node
    public int getyPosition() {
        return yPosition;
    }
    
    //returns true if node is a ground tile (not an obstacle or barricade)
     public boolean isWalkable() {
        return walkable;
    }
     
    //sets if node is a ground tile (not an obstacle or barricade)
    public void setWalkable(boolean walkable) {
        this.walkable = walkable;
    }
    
    //returns the previous node
    public AbstractNode getPrevious() {
        return previous;
    }
     
    //sets the previous node
    public void setPrevious(AbstractNode previous) {
        this.previous = previous;
    }
    
    //gets the fCost (the best estimate of the cost of the path going through the current node)
    public int getfCosts() {
        return gCosts + hCosts;
    }
    
    //gets the gCost (cost of getting from the start node to the current node)
    public int getgCosts() {
        return gCosts;
    }
     
    //sets the gCost (cost of getting from the start node to the current node)
    private void setgCosts(int gCosts) {
        this.gCosts = gCosts;
    }
    
    //sets the gCost (cost of getting from the start node to the current node)
    public void setgCosts(AbstractNode previousAbstractNode, int basicCost) {
        setgCosts(previousAbstractNode.getgCosts() + basicCost);
    }
    
    //sets the gCost (cost of getting from the start node to the current node)
    public void setgCosts(AbstractNode previousAbstractNode) {
        if (diagonally) {
            setgCosts(previousAbstractNode, DIAGONALMOVEMENTCOST);
        } else {
            setgCosts(previousAbstractNode, BASICMOVEMENTCOST);
        }
    }
    
    //calculates the g cost (cost of getting from the start node to the current node)
    public int calculategCosts(AbstractNode previousAbstractNode, int movementCost) {
        return (previousAbstractNode.getgCosts() + movementCost);
    }
    
    //calculates the g cost (cost of getting from the start node to the current node)
    public int calculategCosts(AbstractNode previousAbstractNode) {
        if (diagonally) {
            return (previousAbstractNode.getgCosts()
                    + DIAGONALMOVEMENTCOST);
        } else {
            return (previousAbstractNode.getgCosts()
                    + BASICMOVEMENTCOST);
        }
    }
    
    //gets the h cost (estimated cost from the current node to the goal node)
    public int gethCosts() {
        return hCosts;
    }
    
    //sets the h cost (estimated cost from the current node to the goal node)
    protected void sethCosts(int hCosts) {
        this.hCosts = hCosts;
    }
    
    public abstract void sethCosts(AbstractNode endAbstractNode);

    @Override
    public String toString() {
        return "(" + getxPosition() + ", " + getyPosition() + "): h: "
                + gethCosts() + " g: " + getgCosts() + " f: " + getfCosts();
    }
}
