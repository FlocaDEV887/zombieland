/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flocadev.zombieland;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 *
 * @author nickm2422
 */

/*
 * The main player class. This class handles everything that has to do with
 * the player including vertical and horizontal movements, player 360 rotation,
 * adding and subtracting health, shooting, player gun and player interactions
 * with zombies, barricades, and obstacles
 */

public class Player implements InputProcessor{
    
    //Constants
    public static final float SIZE = 1f;
    public static final float SPEED = 5f;
    private static final float MAX_VELOCITY = 5f;
    
    private Rectangle bounds;   //for collisions
    
    private Vector2 position, velocity; //for movement
    
    private float direction;    //for shooting and animations
    private float stateTime = 0f;   //timer used for animations
    private float camX, camY;   //variables are used in orthographic camera
    
    private double health;
    
    private Gun gun;
    
    private boolean shoot = false;
   
    private long score; //a long to store larger numbers (could be unnecessary, but Chuck Norris might be playing)
    
    public Player(float x, float y)
    {
        health = 100;
        bounds = new Rectangle (x,y,SIZE, SIZE);
        position = new Vector2(x,y);
        direction = getDirection();
        velocity = new Vector2(0, 0);
        score = 0;
        giveGun(Gun.Type.Colt1911); //default gun
    }
    
    public void updateCamPos(float x, float y)
    {
        camX = x;
        camY = y;
    }
    
    //Player obtains a gun (he spawns with one, or gets a new one somehow)
    public void giveGun(Gun.Type type)   {
        gun = new Gun(type);
    }
    
    // creates the bullet and determines the direction in which it needs to be shot
    public Bullet shoot()
    {
        // if the player has bullets and the shot delay permits him to shoot then create the bullet
        if (gun.getShotDelay() <= 0 && gun.getBulletsInClip() > 0 && gun.getReloadTimer() <= 0)   {
            Bullet bullet = new Bullet(position.x + SIZE*0.5f, position.y + SIZE*0.5f, getDirection());
            gun.setShotDelay();
            gun.setBulletsInClip();
            return bullet;
        }else    
        {
            return null;
        }
    }
    
    // subtract health from the player when the zombie attacks him
    public void subtractHealth(double damage)
    {
        if(health > 0)
        {
            health -= damage;
        }
    }
    
    // add health to the player so he can gradually revive himself
    public void addHealth(double healthGained)
    {
        health += healthGained;
    }
    
    // Setters and Getters:
    
    public double getHealth()  {
        return health;
    }
    
    public float getDirection()
    {
        return direction-90f;
    }
    
    public float getX()
    {
        return position.x;
    }
    
    public float getY()
    {
        return position.y;
    }
    
    public long getScore()  {
        return score;
    }
    
    //If the developers want to spawn/teleport the player anywhere
    public void setX(float x)
    {
        position.x = x;
    }
    
    public void setY(float y)
    {
        position.y = y;
    }
    
     public void setXVel(float v)
    {
        velocity.x = v;
    }
    
    public void setYVel(float v)
    {
        velocity.y = v;
    }
    
    public float getXVel()
    {
        return velocity.x;
    }
    
    public float getYVel()
    {
        return velocity.y;
    }
    
    public float getStateTime()
    {
        return stateTime;
    }
    
    public void setScore(long s)  {
        score = s;
    }
    
    public void addScore(long s)  {
        score += s;
    }

    // updates the player's position based on its resent movements
    public void update(float delta) 
    {
        stateTime += delta;
        
        // can't run any faster then allowed
        if(velocity.x > MAX_VELOCITY)
        {
            velocity.x = MAX_VELOCITY;
        }
        if(velocity.x < -MAX_VELOCITY)
        {
            velocity.x = -MAX_VELOCITY;
        }
        
        //Stop the player from going off the screen (to where the zombies spawn)
        if (position.x < 1 || position.x > 31)  {
            velocity.x = 0;
            if (position.x < 1)
                position.x = 1;
            else
                position.x = 31;
        }
        
        if (position.y < 1 || position.y > 33)  {
            velocity.y = 0;
            if (position.y < 1)
                position.y = 1;
            else
                position.y = 33;
        }
        
        //Update gun
        if (gun != null)
        {
            gun.update(delta);
        }
        
        // adds the velocity to player's position based on time
        position.add(velocity.x*delta, velocity.y*delta);
    }
    
    public Gun getGun() {
        return gun;
    }
    
    //Collision rectangle
    public Rectangle getRect(){
        Rectangle rect;
        rect = new Rectangle(position.x - (SIZE/2), (position.y - (SIZE/2)), SIZE, SIZE);
        return(rect);
    }
    
    // determines if the player is touching the barricade
    public boolean isTouchingBarricade(Barricade b)
    {
        boolean collision = false;
        // get the barricade's x and y and subtract it from the player's x and y
        float nearX = getX() - b.getX();
        float nearY = getY() - b.getY();
        
        // essentially if the barricade and the player are touching each other there is a collision
        if(nearX <= 1.1 && nearX >= -1.1 && nearY <= 1.1 && nearY >= -1.1)
        {
            collision = true;
        }
        
        return(collision);
    }
    
    @Override
    // handles rotation when player is walking
    public boolean mouseMoved(int screenX, int screenY) {
        //get x and y position of the mouse
        double x = screenX/MapRenderer.ppuX + camX;
        double y = MapRenderer.camHeight - screenY/MapRenderer.ppuY + camY;
        
        //player position (as a double)
        double xPos = (double)(position.x);
        double yPos = (double)(position.y);
        
        //determine the angle between the player and the mouse to see which direction the player is facing
        if(x < xPos + SIZE/2) //if the mouse is to the left of the player (Quadrant 2 and 3)
        {
            direction = (int)(180 - (Math.atan((y-yPos-SIZE/2)/(xPos+SIZE/2-x)))*180/Math.PI);
        
        }else if (y < yPos + SIZE/2 && x > xPos + SIZE/2) //if the mouse is below the player and to the right 
        {
            direction = (int)(360 + (Math.atan((y-(yPos+SIZE/2))/(x-(xPos+SIZE/2))))*180/Math.PI);
        
        }else if (x > xPos + SIZE/2 && y > yPos + SIZE/2) //if the mouse is above and to the right of the player 
        {
            direction = (int)((Math.atan((y-(yPos+SIZE/2))/(x-(xPos+SIZE/2))))*180/Math.PI);
        }
        return true;
    }
    
    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }
    
    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }
    
    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    // handles the player rotation when gun is being shot
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        //get x and y position of the mouse
        double x = screenX/MapRenderer.ppuX + camX;
        double y = MapRenderer.camHeight - screenY/MapRenderer.ppuY + camY;
        
        //player position (as a double)
        double xPos = (double)(position.x);
        double yPos = (double)(position.y);
        
        //determine the angle between the player and the mouse to see which direction the player is facing
        if(x < xPos + SIZE/2) //if the mouse is to the left of the player (Quadrant 2 and 3)
        {
            direction = (int)(180 - (Math.atan((y-yPos-SIZE/2)/(xPos+SIZE/2-x)))*180/Math.PI);
        
        }else if (y < yPos + SIZE/2 && x > xPos + SIZE/2) //if the mouse is below the player and to the right 
        {
            direction = (int)(360 + (Math.atan((y-(yPos+SIZE/2))/(x-(xPos+SIZE/2))))*180/Math.PI);
        
        }else if (x > xPos + SIZE/2 && y > yPos + SIZE/2) //if the mouse is above and to the right of the player 
        {
            direction = (int)((Math.atan((y-(yPos+SIZE/2))/(x-(xPos+SIZE/2))))*180/Math.PI);
        }
        return true;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
    
    
}
