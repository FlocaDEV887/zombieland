/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flocadev.zombieland;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author nickm2422
 */

/*
 * This is called when the shot method is called (an iterator of bullets is used).
 * The bullet will fly in a certain direction at a certain speed up to a specific 
 * maximum range determined by the gun. Upon collision with an obstacle or zombie,
 * or reaching the max range, the bullet will die.
 */

public class Bullet {
    
    // velocity is based on a calculation using speed
    private Vector2 position, velocity;
    
    private float direction;
    public static final float SIZE = 0.25f;
    private float speed = 0.8f;
    
    public Bullet (float x, float y, float d)
    {
        position = new Vector2(x,y);
        direction = getDirection(d);
        velocity = new Vector2();
    }
    
    // Setters and Getters:
    
    public float getX()
    {
        return position.x;
    }
    
    public float getY()
    {
        return position.y;
    }
    
    //sets the direction of the bullet according to the direction the player is facing
    public float getDirection(float d)
    {
        return direction = d + 90f;
    }
    
    public float setXVelocity()
    {
        velocity.x = (float)(speed*Math.cos((direction*Math.PI)/180f));
        return velocity.x;
    }
    
    public float setYVelocity()
    {
        velocity.y = (float)(speed*Math.sin((direction*Math.PI)/180f));
        return velocity.y;
    }
    
    public float setXPosition(float x)
    {
        return position.x = x;
    }
    
    public float setYPosition(float y)
    {
        return position.y = y;
    }
    
    // applys the movements applied to the current bullet
    public void update(float delta)
    {
        //moves the bullet at the set speed
        position.x += setXVelocity();
        position.y += setYVelocity();
    }
    
    // Collision rectangle
    public Rectangle getRect()
    {
        Rectangle rect;
        rect = new Rectangle(position.x, position.y, SIZE, SIZE);
        return(rect);
    }
}
