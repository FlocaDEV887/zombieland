/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flocadev.zombieland;

import com.badlogic.gdx.Gdx;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author nickm2422
 */

/*
 * This class defines what the world/map will look like (i.e. the position of
 * the barricades and obstacles). It also specifies where the player spawns, as
 * well as containing the algorithm for randomly spawning zombies. This class deals
 * with gun upgrades as well (since it is linked to the current wave)
 */

public class MapLayout {
    
    private int wave = 0;   //zombie wave; set the value 1 lower than the starting wave
    private Tile[][] grid;
    private Player player;
    private float spawnTime = 0;    //Delay between each spawning zombies
    private int zombiesToSpawn = 0; //How many zombies to spawn according to the wave
    private float waveDelay = 0;    //Delay between the end of the wave and the start of the next one
    private Gun gun;                //Only used in here for instantiating and upgrading the gun
    private boolean nextWave = false;
    private int upgradeLevel = 0;   //Used to determine which gun is the next upgrade
    private boolean nextUpgrade = false;
    
    private ArrayList<Zombie> zombies = new ArrayList<Zombie>();
    private ArrayList<Bullet> bullets = new ArrayList<Bullet>();
    private ArrayList<Tile> obstacles = new ArrayList<Tile>();
    private ArrayList<Tile> barricades = new ArrayList<Tile>();
    
    public MapLayout()
    {
        player = new Player(5, 29);
        grid = new Tile[33][35];
        gun = new Gun(Gun.Type.Colt1911);   //Default starting gun is the Colt1911

        /*
         * Inner obstacles are labeled as numbers:
         * see the paper where the rough outline of the map
         * was drawn. Numerous for loops are being used to keep
         * the code as concise as possible
         */
        
        //Outside wall
        
        //Top and bottom
        for (int i = 5; i < 9; i++)     {
            obstacles.add(grid[i][4] = new Obstacle(i, 4));
            obstacles.add(grid[i][30] = new Obstacle(i, 30));
        }
        
        for (int i = 9; i < 12; i++)    {
            obstacles.add(grid[i][30] = new Obstacle(i, 30));
        }
        
        for (int i = 11; i < 14; i++)   {    
            obstacles.add(grid[i][4] = new Obstacle(i, 4));
        }
        
        for (int i = 14; i < 26; i++)   {
            obstacles.add(grid[i][30] = new Obstacle(i, 30));
            obstacles.add(grid[i][4] = new Obstacle(i, 4));
        }
        //Left wall
        for (int i = 4; i < 20; i++)    {
            obstacles.add(grid[4][i] = new Obstacle(4, i));
        }
        for (int i = 22; i < 31; i++)   {
            obstacles.add(grid[4][i] = new Obstacle(4, i));
        }
        //Right wall
        for (int i = 11; i < 15; i++)
            obstacles.add(grid[25][i] = new Obstacle(25, i));
        for (int i = 17; i < 28; i++)
            obstacles.add(grid[25][i] = new Obstacle(25, i));
        
        //Gun room
        
        //Top and bottom
        for (int i = 26; i < 31; i++)   {
            obstacles.add(grid[i][4] = new Obstacle(i, 4));
            obstacles.add(grid[i][11] = new Obstacle(i, 11));
        }
        
        //Right wall
        for (int i = 4; i < 12; i++)
            obstacles.add(grid[31][i] = new Obstacle(31, i));
        
        //Obstacle 1:
        for (int i = 7; i < 10; i++)    {
            obstacles.add(grid[i][26] = new Obstacle(i, 26));
            obstacles.add(grid[i][27] = new Obstacle(i, 27));
        }
        
        //Obstacle 2:
            obstacles.add(grid[10][29] = new Obstacle(10, 29));
            obstacles.add(grid[11][29] = new Obstacle(11, 29));
            
        //Obstacle 3:
            obstacles.add(grid[5][24] = new Obstacle(5, 24));
            obstacles.add(grid[6][24] = new Obstacle(6, 24));
            
        //Obstacle 4:
        for (int i = 13; i < 17; i++)   {
            for (int j = 17; j < 25; j++)
                obstacles.add(grid[i][j] = new Obstacle(i, j));
        }
        
        //Obstacle 5:
        for (int i = 24; i < 28; i++)   {
            obstacles.add(grid[21][i] = new Obstacle(21, i));
            obstacles.add(grid[22][i] = new Obstacle(22, i));
        }
        
        //Obstacle 6:
        for (int i = 8; i < 10; i++)    {
            obstacles.add(grid[i][9] = new Obstacle(i, 9));
            obstacles.add(grid[i][13] = new Obstacle(i, 13));
            obstacles.add(grid[i][14] = new Obstacle(i, 14));
        }
            obstacles.add(grid[10][9] = new Obstacle(10, 9));
            obstacles.add(grid[10][14] = new Obstacle(10, 14));
        for (int i = 9; i < 15; i++)
            obstacles.add(grid[7][i] = new Obstacle(7, i));
        
        //Obstacle 7:
        for (int i = 17; i < 22; i++)
            obstacles.add(grid[i][11] = new Obstacle(i, 11));
        for (int i = 11; i < 15; i++)
            obstacles.add(grid[22][i] = new Obstacle(22, i));
        
        //Obstacle 8:
        for (int i = 12; i < 15; i++)   {
            obstacles.add(grid[i][5] = new Obstacle(i, 5));
            obstacles.add(grid[i][6] = new Obstacle(i, 6));
        }
        
        //Obstacle 9:
        for (int i = 25; i < 28; i++)   {
            obstacles.add(grid[i][5] = new Obstacle(i, 5));
            obstacles.add(grid[i][6] = new Obstacle(i, 6));
        }
        
        //Obstacle 10:
        for (int i = 25; i < 31; i++)   {
            if (i < 28)
                obstacles.add(grid[i][9] = new Obstacle(i, 9));
                obstacles.add(grid[i][10] = new Obstacle(i, 10));
        }
        
        /*
         * Barricades being added in are labeled as numbers:
         * see paper.
         */
        
        //Barricade 1:
        barricades.add(grid[9][4] = new Barricade(9, 4));
        barricades.add(grid[10][4] = new Barricade(10, 4));
        
        //Barricade 2:
        barricades.add(grid[4][20] = new Barricade(4, 20));
        barricades.add(grid[4][21] = new Barricade(4, 21));
        
        //Barricade 3:
        barricades.add(grid[12][30] = new Barricade(12, 30));
        barricades.add(grid[13][30] = new Barricade(13, 30));
        
        //Barricade 4:
        barricades.add(grid[25][28] = new Barricade(25, 28));
        barricades.add(grid[25][29] = new Barricade(25, 29));
        
        //Barricade 5:
        barricades.add(grid[25][15] = new Barricade(25, 15));
        barricades.add(grid[25][16] = new Barricade(25, 16));
    }
    
    //sets the number of zombies in the next wave
    public void nextWave()   {
        wave++;
        //Algorithm used to determine how many zombies to spawn according
        //to the wave
        zombiesToSpawn = 4 + 6*wave;
        
        spawnZombie(Gdx.graphics.getDeltaTime());
        
        //Give the option to upgrade at waves 5, 10, and 15
        if (wave == 5 || wave == 10 || wave == 15)  {
            nextUpgrade = true;
            //upgradeLevel will increase, indicating which gun the play can upgrade
            //to (i.e. The higher the wave, the better the gun)
            upgradeLevel++;
        }
    }
    
    private void spawnZombie(float delta) {
        if(zombiesToSpawn == 0)
        {
            nextWave = true;
            return;
        }
        zombiesToSpawn--;
        spawnTime = 0;
        
            //determine whether zombie spawns on horizontal or vertical axis of screen:
            //if 1, horizontal, if 2, vertical
            int selectAxis = (int)(Math.random()*2);
            if (selectAxis == 1)    {   //horizontal axis
                float zX = Math.round((float)(Math.random()*32));   //random int between 0 and 32
                //System.out.println("Zombie x: " + zX);  //Testing purposes
                //We can reuse selectAxis to decide whether to spawn the zombie
                //up top or on the bottom of the horizontal axis
                selectAxis = (int)(Math.random()*2);
                //Let the top axis value be 2, the bottom be 1
                if (selectAxis == 1)
                    zombies.add(new Zombie(zX, 0, player));
                else
                    zombies.add(new Zombie(zX, 34, player));
            }
            else    {   //vertical axis
                float zY = Math.round((float)Math.random()*34); //random int between 0 and 34
                //System.out.println("Zombie y: " + zY);  //Testing purposes
                //Reusing the selectAxis variable again, this time to determine 
                //whether the zombie will spawn on the left or right side of the screen
                selectAxis = (int)(Math.random()*2);
                //Let the left value be 1, the right be 2
                if (selectAxis == 1)
                    zombies.add(new Zombie(0, zY, player));
                else    {
                    //Zombies cannot spawn right outside the gun room, because that
                    //is out of bounds (as well as the tiles just outside, to avoid 
                    //collision glitches
                    while (zY <= 12 && zY >= 3) {   //the area just outside the gun room
                        zY = Math.round((float)Math.random()*34); //random int between 0 and 34
                        //This loop is repeated until zY's value is outside of the bounds
                        //outside the gun room
                    }
                    zombies.add(new Zombie(32, zY, player));
                }
            }
            
            int label = 0;  //label the zombies to keep track of them easier in the output
            for (Zombie z: zombies) {
                z.setState(Zombie.State.OUTSIDE);   //Since they just spawned
                z.setLabel(label);
                label++;
            }
    }
    
    //Handles the time for the delay between waves
    public void waveDelay(float delta)
    {
        if (waveDelay < 15 && nextWave == true) {
            waveDelay+= delta;
        }
        if(waveDelay >= 15)
        {
            nextWave();
            waveDelay = 0;
            nextWave = false;
        }
            
    }
    
    //Called when the player shoots (clicks LMB)
    public void createBullet()
    {
        Bullet b = player.shoot();  //Determines whether the bullet can actually be created
        if (b != null)
            bullets.add(b);
    }
    
    //Mostly deals with timers between zombie spawning
    public void update(float delta) {
        /*
         * Basically, as the the wave increases (and the difficulty increases),
         * the amount of time between zombies spawning reduces until it reaches
         * wave 20, at which point 0.2s between each zombie is sufficient to 
         * give the player a good challenge
         */
        spawnTime += delta;
        if (wave < 20)
        {
            if (spawnTime > 3.140 - 0.140*wave)
            {
                spawnZombie(delta);
            }
        }
        else
        {
            if (spawnTime > 0.2)
            {
                spawnZombie(delta);
            }
        }
        waveDelay(delta);
    }
    
    //Once the player has pressed Shift to upgrade their gun (note: the upgradeLevel
    //variable does not decrease or reset, it only increases,as it represents a
    //better gun upgrade)
    public void finishUpgrade()   {
        nextUpgrade = false;
    }
    
    public Tile getTile(int x, int y)   {
        if ((x < 0 || x > 32) || (y < 0 || y > 34))
            return null;
        else
            return grid[x][y];
    }
    
    public Tile setTile(int x, int y, Tile tile)   {
        grid[x][y] = tile;
        return grid[x][y];
    }
    
    //Width of the world
    public int getWidth()   {
        return grid.length;
    }
    
    //Height of the world
    public int getHeight()  {
        return grid[0].length;
    }
    
    public int getWave()    {
        return wave;
    }
    
    //To determine the next gun upgrade
    public int getUpgradeLevel()   {
        return upgradeLevel;
    }
    
    //This method determines each gun's upgrade cost
    public long upgradeCost()   {
        /*
         * Given the costs below to upgrade, the player won't be able to upgrade
         * to the gun below right at the beginning of the wave required to upgrade
         * to that gun; thus the upgrade is expensive, and causes the player to be
         * more economical about spending their points
         */
        if (upgradeLevel == 1)
            return 900;
        else if (upgradeLevel == 2)
            return 2500;
        else if (upgradeLevel == 3)
            return 5200;
        else
            return 0;
    }
    
    public boolean getUpgrade() {
        return nextUpgrade;
    }
    
    //This method turns the wave number into a roman numeral.
    //It is used in the top-left status ingame, for the player to see the wave number
    public String getWaveAsNumeral()    {
        int num = wave;     //storage variable to convert to roman numerals
        String s = "";      //this will be the roman numeral itself
        if (num < 1)
            return s;
        while (num >= 1000) {
            s += "M";
            num -= 1000;        
        }
        while (num >= 900) {
            s += "CM";
            num -= 900;
        }
        while (num >= 500) {
            s += "D";
            num -= 500;
        }
        while (num >= 400) {
            s += "CD";
            num -= 400;
        }
        while (num >= 100) {
            s += "C";
            num -= 100;
        }
        while (num >= 90) {
            s += "XC";
            num -= 90;
        }
        while (num >= 50) {
            s += "L";
            num -= 50;
        }
        while (num >= 40) {
            s += "XL";
            num -= 40;
        }
        while (num >= 10) {
            s += "X";
            num -= 10;
        }
        while (num >= 9) {
            s += "IX";
            num -= 9;
        }
        while (num >= 5) {
            s += "V";
            num -= 5;
        }
        while (num >= 4) {
            s += "IV";
            num -= 4;
        }
        while (num >= 1) {
            s += "I";
            num -= 1;
        }    
        return s;
    }
    
    public Player getPlayer()   {
        return player;
    }
    
    public Gun getGun()         {
        return gun;
    }
    
    //These will be controlled in the WorldController, therefore, we need to be
    //able to access the zombies and other arrays which are spawned here
    public ArrayList<Zombie> getSpawningZombies()    {
        return zombies;
    }
    
    public ArrayList<Bullet> getCreatedBullets()    {
        return bullets;
    }
    
    public ArrayList<Tile> getCreatedObstacles()    {
        return obstacles;
    }
    
    public ArrayList<Tile> getCreatedBarricades()    {
        return barricades;
    }
}
