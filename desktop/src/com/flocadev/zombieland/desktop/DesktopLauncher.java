package com.flocadev.zombieland.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.flocadev.zombieland.MainGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
                config.title = "Zombie Land";
                config.width = 800;
                config.height = 480;
		new LwjglApplication(new MainGame(), config);
	}
}