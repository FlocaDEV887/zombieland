Zombieland 
By Matthew Richards, Matt Nickel and Catalin Floca-Maxim

1.	Game Overview
•	In Zombieland a player starts off with a pistol (Colt1911) and is in a small square room.  Suddenly, zombies start to break in, slowly at first, but come in more frequently wave after wave.  The player’s goal in this game is to kill incoming zombies to gain points and ultimately survive each incoming wave of zombies.
•	Zombieland includes zombie intelligence in the respect of zombies spawning randomly on outside of the room and will proceed to one of the 5 barricades, break it and find the best path to attack the player while maneuvering around obstacles.
•	The player can also upgrade the starting Colt1911 gun to a better gun with more bullets per clip and faster firing rate every five waves at the cost of some of the player’s points earned from killing zombies.
•	Zombieland’s biggest motto and overall objective of the game is just to survive.

    2.    User Documentation
•	The user can control the player’s vertical and horizontal movements by pressing the keys “w” (UP), “s” (DOWN), “a” (LEFT) and “d” (RIGHT).  When the player has no more bullets left in the clip he can press the “r” key to reload. To fix damaged but not broken barricades the player can also press “space” to restore the barricades’ health.  Finally the user rotates the player with the mouse and can shoot the gun with the left click.
•	Technically, the user cannot beat the game but the overall objective of the game is to survive as many waves as possible by killing the attacking zombies by shooting each zombie twice.

    3.    Design
•	Classes:
o	Player - controls the player movements, rotation, shooting, health, score and player interactions with zombies and barricades.
o	Zombie - controls the zombie movements, rotation, attacking, health, state and current objective and zombie interactions with player, barricades and bullets.
o	MapRenderer - controls the drawing of everything used in the game including player, zombies, tiles, barricades, obstacles, status bar, player and zombie walking movements and different screens. 
o	WorldController - controls the different rules and is the overall logic of the game.  WorldController includes key inputs WorldController includes dealing with key inputs, zombie intelligence in moving to the player and collisions between player, obstacles, zombies, bullets and barricades.
o	MapLayout - controls the way the map is drawn, where everything is drawn like the player, barricade, spawning zombies, obstacles and newly created bullets.
o	Bullet - called when the shot method is called. The bullet will fly in a certain direction at a certain speed up to a specific maximum range determined by the gun. Upon collision with an obstacle or zombie, the bullet will die.
o	Gun - contains all the specifics of the gun that the player possesses. This class controls certain elements such as reloading, when a player is allowed to shoot, etc..
o	Tile - the master class of obstacles and barricades; the world is composed of these tiles, which can be either null tiles (floor), obstacles, or barricades.
o	Obstacle - a subclass of Tile; this a tile in which the player or zombie cannot pass or shoot through. It also cannot be broken. When bullet overlaps with an obstacle the bullet is destroyed
o	Barricade - a subclass of Tile; the player and zombies cannot go through this tile (i.e. collisions exist) while it is in a default or repairing/breaking state; when it is broken, players and zombies can pass through the barricade.
o	MainGame - handles screens and main soundtrack. 
o	AbstractNode - is the main class that does the "behind the scenes" work for the pathfinding for the zombies to find the best path to take to move towards the player.
o	ExampleNode - part of the pathfinding method.
o	ExampleBuilder - part of the pathfinding method. Creates the nodes.
o	NodeBuilder - part of the pathfinding method.
•	Challenges/Struggles:
o	Getting the zombie to use A.I. (Artificial Intelligence) to find the fastest route to the player when the zombie is in an inside state.
o	Getting the zombies to randomly spawn around the outside of the map and move to one of the 5 barricades when in an outside state.
o	Getting the zombies to determine which barricade they are breaking, breaking it down and then moving inside to chase after the player.

   4.     Development Process
•	Process Used:
o	While creating Zombieland, the development process of sprints was used.  The method of sprints was used so the project members can meet in person daily, socialize and start to communicate more effectively than when working together remotely.
o	See Sprints Doc to see the list of sprints each week in the project Zombieland from start to finish.
•	Distribution of Work:
o	To distribute the work without overwriting code, each group member worked in a different area of the project and by the end each area was pulled together create a functional game.
•	Team Development:
o	To make sure the code was kept in sync and two people were not making changes to the same code at the same time, each member texted one another of what they were working on and in what classes.

5.	Lessons Learned
•	In respect to team development vs. individual development, the Zombieland team felt that team development works well because the game can grow faster with more people working on it.  Also each member can focus on their own programming strengths which leads to specific specialists in specific areas of the game.  Finally, when one member is struggling with a problem or glitch in the game than he has two people that can easily help him work through the problem.
•	One of the only downsides to team development is the fact that sometimes one member cannot work in a specific class when they need it due to the fact that another group member is working in that class at that time.